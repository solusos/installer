SolusOS Installer (Valhalla)
=================

Public Involvement
------------------
This repository is currently available publicly so that SolusOS users may see development progress as and when
it happens. It does not exist for the purposes of feature requests. Issues in the current build may be reported,
bugs, etc, but a roadmap is now in place.

Aims
----
To be a lightweight and fully functional operating system installer.
Must be able to install SolusOS from live medium

Build Dependencies for SolusOS Eveline
--------------------------------------
	sudo apt-get install devscripts cmake gir1.0-glib-2.0 libxml2-dev libglib2.0-dev libgtk2.0-dev valac libparted0-dev libunique-dev gir1.0-gtk-2.0 gir1.0-unique-1.0 libgconf2-dev libgeoip-dev

Building the installer for testing
----------------------------------
	mkdir build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX=. ../.
	make
	make install
	sudo LD_LIBRARY_PATH=lib/ ./bin/valhalla

Cleaning up
-----------
	make clean
	cd ../.
	rm -rvf build/

Updating the translations
-------------------------
	mkdir build
	cd build
	cmake ../.
	make pot
	make clean
	cd ..
	rm -rf build/

Current worksheet
---------
 * Recreate installer UI as seen in Python installer (DONE)
 * Port Ubiquity UI widgets to Vala (DONE)
 * Start implementing logic (TODO)
 * Create VAPI for libparted (DONE)
 * Add CMake build system (DONE)


Implementation
--------------

The SolusOS Installer (as yet unnamed) is written entirely in Vala. The previous selection
of installers tried and implemented within SolusOS have been predominantly Python.
As flexible as this language has proven, it has also proven itself to be immensely slow and
inefficient for the task at hand, installing a full Linux distribution.

Vala has been chosen to ensure speed and native library access, such as libparted. By integrating
directly with these libraries we remove the inefficiency and inaccuracy of multiple abstraction
layers.

The toolkit of choice is GTK, as SolusOS is and always will be a GTK based distro.

Author
------
The SolusOS Installer is written entirely by Ikey Doherty, founder of the SolusOS Linux Distribution.
