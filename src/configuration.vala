namespace Installer {

errordomain InvalidConfigError {
	INVALID_CONFIG,
}

// One reference to the installer config
public static Configuration Config;
public static Configuration getConfig() { 
	if (Config == null) {
		try {
			Config = new Configuration("%s/installer.ini".printf(RESOURCE_DIR));
		} catch (InvalidConfigError e) {
			stdout.printf("%s\n", e.message);
			Config = null;
		}
	}
	return Config;
}

public class Configuration {

   // No need for public access, attempt to be a wrapper

   private KeyFile keys;

   // Our groups.
   private HashTable<string, ConfigSection> groups;

   /**
    * Construct a new Configuration manager
    */
   public Configuration (string whence) throws InvalidConfigError {
	keys = new KeyFile ();
	groups = new HashTable<string,ConfigSection>(str_hash,str_equal);

	try {
		keys.load_from_file (whence, KeyFileFlags.NONE);
		foreach (string grp in keys.get_groups ()) {
			ConfigSection c;
			HashTable<string,string> tmp = new HashTable<string,string>(str_hash, str_equal);
			foreach (string key in keys.get_keys (grp)) {
				tmp.insert (key, keys.get_value (grp, key));
			}
			c = new ConfigSection(grp, tmp);
			groups.insert(grp, c);
		}
		// Sanity check
		string[] groups = {"Branding", "ReleaseNotes", "Interface", "Timezone", "Users", "Bootloader", "System" };
		foreach (string group in groups) {
			if ( ! keys.has_group (group) ){
				stdout.printf("Group %s not found\n", group);
				throw new InvalidConfigError.INVALID_CONFIG ("Invalid Configuration File");
			}
		}
	} catch (Error e) {
		throw new InvalidConfigError.INVALID_CONFIG (e.message);
	}
   }

   /**
    * Retrieve the ConfigSection for "section_name"
    */
   public ConfigSection get(string section_name) {
	return groups.lookup(section_name);
   }

   public void get_string (string section, string name, out string ret) {
	try {
		ret = keys.get_value(section, name);
	} catch (GLib.KeyFileError e) {
		// Log it somewhere ?
	}
   }

   public void get_bool(string section, string key, out bool enabled) {
	try {
		enabled = keys.get_boolean(section, key);
	} catch (GLib.KeyFileError e) {
		// Log it somewhere ?
		stdout.printf("%s\n", e.message);
	}
   }
} // End Configuration class

/**
 * Helper, represents each [section] in a KeyFile
 */
public class ConfigSection : GLib.Object {

   /** Name of this Configuration section */
   public string name { public get; construct set; }

   /** Our Key->Value pairs */
   private HashTable<string,string> keys;

   /**
    * Construct a new ConfigSection with the given name
    */
   public ConfigSection (string name, HashTable<string,string>keys) {
	this.name = name;
	this.keys = keys;
   }

   /**
    * Lookup a value using get
    */
   public string get(string key) {
	return keys.lookup(key);
   }
} // End ConfigSection class
} // end namespace
