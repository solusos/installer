using Posix;

namespace Installer {

public class Permissions {

   public static uid_t uid_save = (uid_t)909090;
   private static uid_t uid_nobody = (uid_t)65534; // Drop euid to "nobody"
   //private static uid_t uid_nobody = (uid_t)1000;
   /**
    * Elevate our permissions back to what they originally were (i.e. 0)
    */
   public static bool Elevate () {
	seteuid (uid_save);

	if (geteuid () == uid_save) return true;

	return false;
   }

   /**
    * Drop permissions to nobody
    */
   public static bool Drop () {
	if (uid_save == 909090) 	uid_save = (uid_t)getuid ();
	seteuid (uid_nobody);
	if (geteuid() == uid_nobody) return true;
	return false;
   }

} // End Permissions class


} // End namespace
