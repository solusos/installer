SolusOS Installer Core Modules
------------------------------

These are modules that are dynamically linked to the UI parts needing them.
For instance, an InstallerPage should not directly be accessing core system information, it should be handled and abstracted by a core module.

InstallerPage's go into src/modules/ NOT src/core, for they are dynamically
loaded at run time. This is so that we may enable multiple User Interfaces, i.e GTK and ncurses, without GTK linking unless explicitly required.

