# il8n system shared lib
vala_precompile(INSTALLER_DISKS
"disks.vala"
"schemes.vala"
PACKAGES
gmodule-2.0
gio-2.0
posix
CUSTOM_VAPIS
${CMAKE_SOURCE_DIR}/vapi/config.vapi
${CMAKE_SOURCE_DIR}/vapi/libparted.vapi
${GENERATED_DIR}/core.vapi
DIRECTORY
.
GENERATE_VAPI
../../../GENERATED/disks
OPTIONS
--disable-warnings
)

# il8n system shared lib
vala_precompile(INSTALLER_DISKS_TEST
"test.vala"
PACKAGES
gmodule-2.0
gio-2.0
CUSTOM_VAPIS
${CMAKE_SOURCE_DIR}/vapi/config.vapi
${GENERATED_DIR}/disks.vapi
${CMAKE_SOURCE_DIR}/vapi/libparted.vapi
DIRECTORY
.
OPTIONS
--disable-warnings
)

add_library ("${INSTALLER_NAME}-disks" SHARED ${INSTALLER_DISKS})
set_target_properties("${INSTALLER_NAME}-disks" PROPERTIES 
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)
target_link_libraries ("${INSTALLER_NAME}-disks" "${INSTALLER_NAME}-config")
target_link_libraries ("${INSTALLER_NAME}-disks" ${LIBPARTED_LIBRARIES})
target_link_libraries ("${INSTALLER_NAME}-disks" "${INSTALLER_NAME}-core")

INSTALL (PROGRAMS ${CMAKE_BINARY_DIR}/lib${INSTALLER_NAME}-disks.so DESTINATION lib)

IF (BUILD_TEST)
add_executable ("disktest" ${INSTALLER_DISKS_TEST} )
target_link_libraries ("disktest" "${INSTALLER_NAME}-disks")
ENDIF (BUILD_TEST)
