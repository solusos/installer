
/*
 * disks.vala: Object orientated wrapper around libparted
 * Many calls in libparted should be one-time calls and makes extensive
 * use of pointers..
 * We want solid representation of devices and partitions without the
 * low-level concerns
 */
using Ped;

namespace Installer {

/*
 * Used to query devices on the system
 */
public class DeviceManager {

   public static List<Installer.Device> get_all_devices() {
	Permissions.Elevate ();
	Ped.Device.probe_all ();
	List<Installer.Device> devices = new List<Installer.Device> ();
	unowned Ped.Device dev = null;

	while ( true ) {
		unowned Ped.Device current = null;
		current = Ped.Device.get_next(dev);
		if (current == null) {
			break;
		} else {
			dev = current;
			devices.append(new Installer.Device(dev));
		}
	}
	Ped.Device.free_all ();
	Permissions.Drop ();

	return devices;
   }
} // End DeviceManager class definition

/**
 * Simply represents a Ped.Device
 */
public class Device : GLib.Object {

   public string path { protected set; public get; }
   public bool read_only { protected set; public get; }
   public Installer.Disk disk { protected set; public get; }

   /**
    * Construct a new "Device" object
    */
   public Device(Ped.Device parent) {
	this.path = parent.path;
	if (parent.read_only == 1) {
		this.read_only = true;
	} else {
		this.read_only =  false;
	}
	this.disk = new Installer.Disk(new Ped.Disk(parent));
   }

} // End Device definition

/**
 * Represent Disk
 */
public class Disk: GLib.Object {

   public string path {protected set; public get; }
   public List<Installer.Partition> partitions;
   public string model { protected set; public get; }

   // Total disk size
   public uint64 size { protected set; public get; }

   // Is this an SSD device ?
   public bool ssd { protected set; public get; }

   // Displayable "type" of disk
   public string disk_type { protected set; public get; } 

   public Disk (Ped.Disk parent) {
	this.path = parent.dev.path;

	// Set up partitions
	partitions = new List<Installer.Partition> ();

	// Set the model from the parent
	this.model = parent.dev.model;

	// Add to the list.
	unowned Ped.Partition p = null;
	while ( true ) {
		unowned Ped.Partition current = null;
		current = parent.next_partition(p);
		if (current == null) {
			break;
		} else {
			p = current;
			Installer.Partition tmp = new Installer.Partition(p);
			this.size += tmp.size;
			partitions.append(tmp);
		}
	}
	CompareFunc<Installer.Partition> partcmp = (a,b) => {
		int a1 = (a as Installer.Partition).number;
		int b1 = (b as Installer.Partition).number;

		return (int) (a1 > b1) - (int) (a1 < b1);
	};

	this.partitions.sort(partcmp);

	// Attempt to work out if this is an ssd
	this.ssd = false;
	var node = path.replace("/dev/","");
	var testSSD = "/sys/block/%s/queue/rotational".printf(node);
	if (FileUtils.test (testSSD, FileTest.EXISTS)) {
		File rotational = File.new_for_path (testSSD);
		try {
			var dis = new DataInputStream (rotational.read(null));
			var line = dis.read_line(null,null);
			if (line.strip() == "0") {
				this.ssd = true;
			}
		} catch (GLib.Error e) { // doesn't matter
		}
	}
	this.disk_type = parent.dev.type.to_string (); // not working yet.
   }
} // End Disk definition

/**
 * Represents a partition.
 */
public class Partition : GLib.Object {

   public string path { protected set; public get; }
   public bool active { protected set; public get; }
   public Installer.Filesystem filesystem { protected set; public get; }

   public uint64 size { protected set; public get; }

   public bool physical { protected set; public get; }

   public int number { protected set; public get; }

   public Partition (Ped.Partition parent) {
	this.path = parent.get_path();
	if (parent.is_active () == 1) {
		this.active = true;
	} else {
		this.active = false;
	}

	this.filesystem = Installer.Filesystem.new_for_partition (parent);
	this.physical = true;
	// TODO: Check actual Partitition flags
	if (this.path.contains ("mapper")) this.physical = false;

	if (this.active && this.physical) {
		unowned Ped.Geometry geom = parent.geom;
		unowned Sector length = geom.length;
		long multpl = parent.disk.dev.sector_size;
		this.size = length * multpl;
	} else {
		this.size = -1;
	}
	this.number = parent.num;

   }
} // End Partition class

/**
 * Represents filesystem
 */
public class Filesystem : GLib.Object {

   public int block_size { protected set; public get; }
   public string name { protected set; public get; }

   public Filesystem (string name, int block_size) {
	this.name = name;
	this.block_size = block_size;
   }

   /**
    * Utility function, return correct filesystem or null for Partition class
    */
   public static Filesystem? new_for_partition ( Ped.Partition p) {
	if (p.is_active() == 0) {
		return null;
	}
	if (p.fs_type == null) {
		return null;
	};
	return new Filesystem (p.fs_type.name, p.fs_type.block_sizes);
   }
} // End filesystem definition

} // End namespace
