namespace Installer {

/**
 * Represents a partitioning scheme for a particular disk
 */
public class PartitionScheme : GLib.Object {

   /** Name of this scheme */
   public string name { construct set; public get; }

   /** Construct a new PartitionScheme */
   public PartitionScheme (string? name) {
	GETTEXT_INIT ();
	this.name = name;
   }

   /** Construct a new PartitionScheme for the given disk */
   public static PartitionScheme[] for_disk (Installer.Disk disk) {
	// We need to query the size and what not of the disk in future.
	return new PartitionScheme[] {
		new PartitionScheme (_("Use entire disk")),
		new PartitionScheme (_("Partition manually")) // could probably nuke this in future, demo purpose atm.
	};
   }

} // End class def

} // End namespace Installer
