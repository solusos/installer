using Installer;

public class TestDisks {

   public TestDisks () {
	DeviceManager dm = new DeviceManager (); // blah blah

	List<Device> devices = DeviceManager.get_all_devices();
	foreach (Device d in devices) {
		Disk disk = d.disk;
		uint partCount = disk.partitions.length ();
		stdout.printf("Device Path: %s\n", disk.path);
		if (partCount > 0) {
			stdout.printf("\t%u partitions found\n", disk.partitions.length ());
			foreach (Partition p in disk.partitions) {
				if (! p.active) continue;
				stdout.printf("\t%s\n", p.path);
				if (p.filesystem != null) {
					stdout.printf("\t\tFilesystem: %s\n", p.filesystem.name);
				}
			}
		} else {
			stdout.printf("\tNo partitions found\n");
		}
	}
   }
}

public static void main(string[] args) {
	TestDisks d = new TestDisks ();
}

