namespace Installer {

/**
 * Does exactly what it says on the tin. Helps with locales, etc.
 */
public class LocaleHelper : GLib.Object {


   private HashTable<string,string> countries;
   private HashTable<string,string> languages;
   public List<LocaleDef?> locales;

   public LocaleHelper () {

	// Init ourselves..
	var countryPath = "%s/countries".printf(RESOURCE_DIR);
	var languagePath = "%s/languages".printf(RESOURCE_DIR);
	var localesPath = "%s/locales".printf(RESOURCE_DIR);

	// Run like hell if these don't exist
	assert (FileUtils.test (countryPath, FileTest.EXISTS));
	assert (FileUtils.test (languagePath, FileTest.EXISTS));
	assert (FileUtils.test (localesPath, FileTest.EXISTS));

	File cList = File.new_for_path(countryPath);
	File lList = File.new_for_path(languagePath);

	countries = new HashTable<string,string> (str_hash, str_equal);
	languages = new HashTable<string,string> (str_hash, str_equal);

	// load up these tables with data
	file_to_table (ref countries, ref cList);
	file_to_table (ref languages, ref lList);

	locales = new List<LocaleDef?> ();

	// Now read in the locales
	File localesFile = File.new_for_path(localesPath);
	try {
		string line;
		var dis = new DataInputStream ( localesFile.read(null) );
		while ( (line = dis.read_line (null,null)) != null) {
			if ( ! line.contains("UTF-8") ) {
				continue;
			}
			string language = "";
			string language_code = "";
			string country = "";
			string country_code = "";
			var locale_code = line.replace("UTF-8", "").replace(".", "");
			locale_code = locale_code.strip ();
			if ( locale_code.contains ("_") ) {
				var split = locale_code.split ("_");
				if (split.length == 2) {
					language_code = split[0];
					var tmp = languages.lookup (language_code);
					if (tmp != null) {
						language = tmp;
					} else {
						language = language_code;
					}
					country_code = split[1].down();
					var tmp2 = countries.lookup (country_code);
					if (tmp2 != null) {
						country = tmp2;
					} else {
						country = country_code;
					}
				}
				// Add this to known locales
				var obj = LocaleDef () { language = language, language_code = language_code, country_code = country_code, country = country, locale_code = locale_code};
				locales.append (obj);
			}
		}
	} catch (GLib.Error e) {
		error (e.message);
	}
  }

  /**
   * Utility helper. Converts a file into KEY->VALUE HashTable<string,string>
   * using 'separator' as the key-value separator
   */
  private void file_to_table(ref HashTable<string,string> mapping, ref File from, string separator = "=") {
	try {
		string line;
		var dis = new DataInputStream (from.read(null));
		while ( (line = dis.read_line (null,null)) != null) {
			if ( ! line.contains (separator)) {
				continue;
			}
			string[] splits = line.split(separator, 2);
			mapping.insert(splits[0], splits[1]);
		}
	} catch (Error e) {
		error (e.message);
	}
   }

} // End LocaleHelper class definition

/**
 * Struct to represent locales
 */
public struct LocaleDef {
   public string language;
   public string language_code;
   public string country_code;
   public string country;
   public string locale_code;
}

} // end namespace
