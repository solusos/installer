namespace Installer {

/**
 * Location helper
 */
public class Location : GLib.Object {

   private Iso3166 iso3166;

   public double longitude { public get; public set; }
   public double latitude { public get; public set; }

   public string human_country { public get; protected set; }
   public string country { public get; protected set; }
   public string human_zone { public get; protected set; }
   public string zone { public get; protected set; }
   public string comment { public get; protected set; }

   public int utc_offset { protected set; public get; }

   public Location (string zonetab_line, ref Iso3166 iso3166, ref ZoneDB zdb) {
	this.iso3166 = iso3166;
	latitude = 0;
	longitude = 0; // otherwise it doesn't compile. stupid old valac -_-
	
	string alatitude = "";
	string alongitude = "";
	

	string[] bits = zonetab_line.chomp().split("\t");
	string latlong = bits[1];


	int latlongsplit = index_of_char ('-', latlong);

	if (latlongsplit == -1) {
		latlongsplit = index_of_char ('+', latlong);
	}

	if (latlongsplit != -1) {
		alatitude = latlong.substring(0, latlongsplit);
		alongitude = latlong.substring(latlongsplit);
	} else {
		alatitude = latlong;
		alongitude = "+0";
	}


	country = bits[0];
	var t  = iso3166.names.lookup(country);
	if (t != null) {
		human_country = t;
	} else {
		human_country = country;
	}
	zone = bits[2];
	comment = "";
	var zone_bits = zone.replace("_", " ").split("/");
	human_zone = zone_bits[zone_bits.length - 1];
	int length = bits.length;
	if (length >  3) {
		comment = bits[3];
	} else {
		comment = "";
	}

	latitude = parse_position(alatitude, 2);
        longitude = parse_position(alongitude, 3);

	// Currently required hack. Links aren't yet implemented
	if (zdb[zone] != null) {
		this.utc_offset = zdb[zone].offset;
	} else {
		this.utc_offset = -1000;
	}


   }

  protected double parse_position (string position, int wholedigits) {
	if (position == "" || position.length < 4 ||  wholedigits > 9) {
		return 0.0;
	}
	var wholestr = position.substring(0, wholedigits + 1);
	var fractionstr = position.substring (wholedigits + 1);
	var whole = (float) (wholestr.to_double ());
	var fraction = (float)(fractionstr.to_double ());
	if ( whole >= 0.0) {
		return whole + fraction / Math.pow (10.0, fractionstr.length);
	} else {
		return whole - fraction / Math.pow (10.0, fractionstr.length);
	}
   }

} // End class
} // End namespace
