#if WITH_GEOIP
using GeoIP;
#endif

namespace Installer {

public const string ISO_3166_FILE = "/usr/share/xml/iso-codes/iso_3166.xml";

/**
 * Deals with entries in the iso3166 xml file
 */
public class Iso3166: GLib.Object {

   public HashTable<string,string> names;

   private string tmp = null;

   protected void nodeCallback (string nodeName) { }

   protected void itemsCallback (string nodeName, string key, string value) {
	if (nodeName != "iso_3166_entry") {
		return;
	}
	if (key == "alpha_2_code") {
		tmp = value;
	}
	if ( tmp != null ) {
		// We already found the alpha_2_code
		if ( key == "common_name" ) {
			names.insert (tmp, value);
			tmp = null;
		} else if (key == "name") {
			if (names.lookup (tmp) == null) {
				// Add name instead.
				names.insert(tmp, value);
				tmp = null;
			}
		}	
	}
   }

   public Iso3166() {
	names = new HashTable<string,string> (str_hash,str_equal);
	try {
		XmlHelper h = new XmlHelper ("/usr/share/xml/iso-codes/iso_3166.xml", "iso_3166_entries", nodeCallback, itemsCallback);
	} catch (XmlHelperError e) {
		// raise somewhere ?
		error (e.message);
	}
   }
} // End Iso3166


/**
 * Locations DB
 */
public class LocationDatabase : GLib.Object {

   public List<Location> locations;
   public HashTable<string,List<Location>> cc_to_locs;
   public HashTable<string,Location> tz_to_loc;
   private ZoneDB zdb;

   // Async stuff
   private SourceFunc callback;
   private string current_zone;

   public LocationDatabase () {
	var path = "/usr/share/zoneinfo/zone.tab";
	File tab = File.new_for_path (path);
	var iso = new Iso3166 ();
	locations = new List<Location> ();
	zdb = new ZoneDB ();

	try {
		string line;
		var dis = new DataInputStream (tab.read(null));
		while ( (line = dis.read_line (null,null)) != null) {
			if (line.contains("#")) {
				continue;
			}
			Location c = new Location (line, ref iso, ref zdb);
			locations.append (c);
		}
	} catch (Error e) {
		// ignore for now
		message (e.message);
	}
	cc_to_locs = new HashTable<string,List<Location>> (str_hash, str_equal);
	tz_to_loc = new HashTable<string,Location> (str_hash, str_equal);

	// Hack as hell loop because valac freaks with the pointers
	foreach (Location loc in this.locations) {
		tz_to_loc.insert (loc.zone, loc);
		unowned List<Location> tmp = cc_to_locs.lookup (loc.country);
		if (tmp != null) {
			tmp.append (loc);
		} else {
			cc_to_locs.insert(loc.country, new List<Location>());
			unowned List<Location> tmp2 = cc_to_locs.lookup (loc.country);
			tmp2.append (loc);
		}
	}

	//zdb = null; // Clear some memory

   }


   /**
    * If built with GeoIP, attempt to obtain current country
    */
   public async string get_current_country () {
	this.callback = get_current_country.callback;

	#if WITH_GEOIP
	ThreadFunc<void*> run = () => {
		string ip = null;
		try {
			var host = "checkip.dyndns.com";
			var port = 80;
			var resolver = Resolver.get_default ();
			var addresses = resolver.lookup_by_name (host, null);
			var address = addresses.nth_data (0);

			var client = new SocketClient ();
			var conn = client.connect (new InetSocketAddress (address, port), null); // No Cancellable object
			var message = @"GET / HTTP/1.1\r\nHost: $host\r\n\r\n";
			conn.output_stream.write (message, (size_t)message.length, null);

			var response = new DataInputStream (conn.input_stream);
			var status_line = response.read_line (null,null).strip (); // Not really any use right now but we could check for 200

			string line;
			while ( (line = response.read_line (null,null)) != null) {
				if (line.contains (":") && line.contains ("<html>")) {
					int start = index_of_char (':', line);
					line = line.substring (start + 1).strip ();
					int end = index_of_char ('<', line);
					line = line.substring (0, end);
					ip = line;
					stdout.printf ("%s\n", line);
				}
			}



		} catch (GLib.Error e) { message (e.message) ;}
		if (ip != null) {
			unowned GeoIPTag gi = GeoIP.GeoIP_new (GeoIP.Options.MEMORY_CACHE);
			current_zone = GeoIP_country_code_by_addr(gi, ip);
			GeoIP_cleanup ();

			Idle.add (callback);
		} 
		return null;
	};
	Thread.create<void*> (run, false);
	yield;
	return current_zone;
	#else
	return null; // Return null when GeoIP is not built
	#endif
   }

} // End class definition

/** 
 * Utility method
 */
public static int index_of_char (unichar test, string input) {
	for (int i=0; i < input.length; i++) {
		unichar c = input[i];
		if (c == test) return i;
	}
	return -1;
}

} // End namespace
