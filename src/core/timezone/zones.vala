namespace Installer {

/**
 * Used to parse the timezone (zoneinfo) files
 */
public class ZoneDB {

   private HashTable<string,TimeZone?> zones;

   /** Utility.. */
   protected int string_to_offset (string inways) {
	var splits = inways.split(":");
	string hours_ = splits[0].replace("-","");
	var hours = 0;
	if (hours_ != "0") hours = hours_.to_int() * (60 * 60);
	string minutes_ = splits[1];
	var minutes = -1000;
	if (minutes_ != "0") minutes = splits[1].to_int () * 60; // How many seconds ?

	if (splits[0].contains("-")) {
		// Minus number
		return - (hours + minutes);
	} else {
		return (hours + minutes);
	}
   }

   public TimeZone? get(string zone_name) {
	return this.zones.lookup (zone_name);
   }

   public ZoneDB () {
	string[] zones = new string[] {"asia","backward","europe","northamerica","southamerica","antarctica",
				"australasia", "etcetera", "gmt", "pacificnew", "africa" };

	this.zones = new HashTable<string,TimeZone?> (str_hash, str_equal);

	HashTable<string,string> links = new HashTable<string,string> (str_hash, str_equal);
	foreach (string zone in zones) {
		var path = "%s/timezone/tz_info/%s".printf(RESOURCE_DIR, zone);

		File zoneInfo = File.new_for_path(path);
		try {
			string line;
			var dis = new DataInputStream (zoneInfo.read(null));
			TimeZone? currentZone = null;
			while ( (line = dis.read_line (null,null)) != null) {

				line = line.strip ();
				if ( line[0] == '#' ) continue; // #Comment line
				//if ( ! line.contains ("\t") || ! line.contains (" ")) continue; // Invalid line
				line = line.replace(" ", "\t");
				var splits = line.split ("\t");
				if (splits[0] == "Rule") {
					// Ignore for now.. DST rules.
				} else if (splits[0] == "Zone") {
					// Ideally.. we should be adding last one to list
					if (currentZone != null) {
						this.zones.insert (currentZone.name, currentZone);
					}

					currentZone = TimeZone ();
					currentZone.name = splits[1];
					currentZone.format = splits[4];
				
					if (! line.contains (":") ) continue; // not a valid line for us.
					currentZone.offset = -1000;
					foreach (string i in splits) {
							if (i.contains (":")) {
								currentZone.offset = string_to_offset (i);
								break;
							} 
					}
					// TimeZone!
				} else if (splits[0] == "Link") {
					links.insert (splits[2], splits[1]);
				} else {
					if (splits.length == 4) { // Should check for trailing comments..
						// Now set stuffs (last one) 
						currentZone.format = splits[4];
						foreach (string i in splits) {
								if (i.contains (":")) {
									currentZone.offset = string_to_offset (i);
									break;
								}
						}
					}
					// Continuation
				}
			}

		} catch (Error e) {
			// ignore for now
			error (e.message);
		}
	}
	foreach (unowned string key in links.get_keys ()) {
		unowned string v = links.lookup (key);
		unowned TimeZone? tz = this.zones.lookup(v);
		if (tz != null) {
			TimeZone? tmp = TimeZone ();
			tmp.name = tz.name;
			tmp.format = tz.format;
			tmp.offset = tz.offset;
			this.zones.insert (tmp.name, tmp);
			links.remove(key);
		}
	}

   }
} // End ZoneDB definition


/**
 * Represents a timezone
 */
public struct TimeZone {

   public string name; // Name of this timezone (Europe/London)

   public int offset; // Seconds offset (may be negative) (UTC/GMT)

   public string format;
}
} // End namespace
