using Installer;

public class XMLTest {

   public int code = 0;

   public void nodeCallback(string node) {
	stdout.printf("Node encountered: %s\n", node);
   }

   public void itemsCallback(string node, string key, string value) {
	stdout.printf("%s/%s = %s\n", node, key, value);
   }

   public XMLTest () {
	try {
		XmlHelper h = new XmlHelper ("/usr/share/xml/iso-codes/iso_3166.xml", "iso_3166_entries", nodeCallback, itemsCallback);
	} catch (XmlHelperError e) {
		code = -1;
	}
   }
}

public static int main(string[] args) {
	XMLTest t = new XMLTest ();

	return t.code;
	// Simple test.
}
