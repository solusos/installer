using Xml.XPath;
using Xml;

namespace Installer {

/**
 * Every time we encounter an attribute, call the delegate
 */
public delegate void AttributeHandler (string node_name, string key, string value);

/**
 * Not much to look at, but just informs the using library that a new node was encountered
 */
public delegate void NodeHandler (string node_name);

public errordomain XmlHelperError {
	ROOT_NOT_FOUND,
	FILE_NOT_FOUND,
	LIBXML_ERROR,
}
public class XmlHelper {

   /**
    * Create a new XmlHelper
    */
   public XmlHelper (string file, string rootNode, NodeHandler nodeH = null, AttributeHandler attributeH) throws XmlHelperError {
	Parser.init ();

        Xml.Doc* doc = Parser.parse_file (file);
        if(doc==null) throw new XmlHelperError.FILE_NOT_FOUND("failed to read the .xml file");
        
        Context ctx = new Context(doc);
        if(ctx==null) throw new XmlHelperError.LIBXML_ERROR("failed to create the xpath context");
        
        Xml.XPath.Object* obj = ctx.eval_expression("/%s".printf(rootNode) );
        if(obj==null) throw new XmlHelperError.ROOT_NOT_FOUND("failed to evaluate xpath");

        Xml.Node* node = null;
        if ( obj->nodesetval != null && obj->nodesetval->item(0) != null ) {
                node = obj->nodesetval->item(0);
        } else {
                throw new XmlHelperError.ROOT_NOT_FOUND("failed to find the expected node\n");
        }

	for (Xml.Node* iter = node->children; iter != null; iter = iter->next) {
		if (iter->type != ElementType.ELEMENT_NODE) {
			continue;
		}
		if (nodeH != null) nodeH (iter->name);

	        Xml.Attr* attr = null;
	        attr = iter->properties;

	        while ( attr != null )
	        {
			attributeH (iter->name, attr->name, attr->children->content);
	                attr = attr->next;
	        }
	}
        delete obj;
        delete doc;
   }

} // End XmlHelper class definition

} // End namespace definition
