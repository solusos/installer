using Gtk;
using Installer;
using Unique;
using Posix;

/**
 * Main entry into the SolusOS Installer
 */
public static int main (string[] args) {


	GETTEXT_INIT ();


	if (Posix.getuid() != (uid_t)0) {
		GLib.stdout.printf ("Installer must be run as root!\n");
		return -1;
	}


	getConfig (); // Need to pre-init always

	Intl.bindtextdomain( GETTEXT_PACKAGE, LOCALE_DIR );
	Intl.textdomain( GETTEXT_PACKAGE );
	Intl.bind_textdomain_codeset( GETTEXT_PACKAGE, "UTF-8" );

	Gtk.init (ref args);

	Unique.App app;
	app = new Unique.App("com.solusos.installer", null);

	if(app.is_running) { //not starting if already running
		Unique.Command command;
		Unique.Response response;
		Unique.MessageData message;
		message = new MessageData ();
		command = (Unique.Command) Unique.Command.ACTIVATE;
		response = app.send_message (command, message);

		if(response == Unique.Response.OK)
			return 0;
		else
			return 1;
	}

	InstallerWindow iw = new InstallerWindow ();


	message ( _("Installer started") );
	Permissions.Drop ();
	Gtk.main();


	return 0;
}
