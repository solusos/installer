namespace Installer {

public class InstallerModule : TypeModule {

   [CCode (has_target = false)]
   private delegate Type PluginInitFunc(TypeModule module);
        
   private GLib.Module module = null;
        
   private string name = null;

   // Our page number in the UI
   public uint page_number { public set; public get; }

   // What we own.
   public InstallerPage payload = null;

   public InstallerModule(string name) {
                this.name = name;
   }
        
   public override bool load() {
	string path = Module.build_path(MODULE_DIR, name);
	module = Module.open(path, GLib.ModuleFlags.BIND_LAZY);
	if(null == module) {
		error("Module not found");
	}

	void * plugin_init = null;
	if(! module.symbol("installer_module_init", out plugin_init)) {
		error("No such symbol");
	}

	((PluginInitFunc) plugin_init)(this);

	return true;
   }
        
   public override void unload() {
	module = null;

	message("Library unloaded");
   }

   

   public InstallerPage make_new (string className) {
	this.payload = (InstallerPage) GLib.Object.new(Type.from_name(className));
	return this.payload;
   }

} // End InstallerModule class definition

} // End namespace
