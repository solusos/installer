using Gtk;

namespace Installer {

public delegate void DataCallback (int index);

public abstract class InstallerPage : GLib.Object {

   /** Init the page */
   public abstract void init_page ();

   /** Get the long title (i.e. window top) */
   public abstract string get_title ();

   /** Get the short title (i.e. window sidebar, ticks) */
   public abstract string get_short_title ();

   /** Get the main widget associated with this page */
   public abstract Gtk.Widget get_display ();

   /** What section of the installer we should be in */
   public abstract string get_section ();

   /** Indicate whether to disable or or enable navigation */
   public signal void navigate_update(NavigationState state, NavigationDirection direction);

   /** Updates the progress of a page */
   public signal void progress_update (ProgressState state, string message = null, float fraction = null);

   /** What type of page are we ? */
   public abstract PageType get_page_type ();

   /** Only interested subclasses would implement this method */
   public virtual void process_data (string source, string? data, DataCallback cb, int index) {}

   /** only Data pages should override this function */
   public virtual string get_data () { return null; }

} // End InstallerPage interface definition

/**
 * Disable or enable the given NavigationDirection
 */
public enum NavigationState {
	DISABLE,
	ENABLE
}

/**
 * Disable or enable FORWARD or PREVIOUS button ?
 */
public enum NavigationDirection {
	FORWARD,
	PREVIOUS,
}

/**
 * Show or hide the progress dialog
 */
public enum ProgressState {
	SHOW,
	HIDE
}

/**
 * Tells the installer what type of page this is
 */
public enum PageType {
	DATA,		// Data collection page
	PRESENTATION,	// Presentation page (no data)
	SUMMARY,	// Summary page
	INSTALLATION,	// Actual installation page
	COMPLETION,	// Page to show upon completion
}

} // End namespace
