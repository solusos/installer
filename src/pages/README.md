SolusOS Installer Modules (UI)
==============================

These modules are dynamically loaded by the installer at run time, and are meant to present a UI on top of options provided src/ and src/core

All modules here must be linked against libinstallerpage and libsolusos-installer-config
