using Gtk;
using Installer.Widgets;

namespace Installer {

public class DisksPage : InstallerPage {

   private Gtk.VBox display;
   private string title;
   private string short_title;
   private bool already_init = false;

   private List<Device> devices;
   private SegmentedBar currentDisk;
   private DiskChooser disk_chooser;

   // Somewhere to display partitioning schemes
   private Gtk.VBox schemes;

   static construct {

	message ("DisksPage init");
   }

   static ~DisksPage() {
	message ("DisksPage destroyed");
   }

   private Installer.Widgets.Color random_color ( ) {
	double red = Random.double_range (0.0, 1.0);
	double green = Random.double_range (0.0, 1.0);
	double blue = Random.double_range (0.0, 1.0);

	return new Installer.Widgets.Color (red, green, blue);
   }

   private void set_bar_from_disk (Disk disk) {
	currentDisk.remove_all ();
	currentDisk.disk_size = 0;
	foreach (Partition part in disk.partitions) {
		if ( ! part.active ) continue;
		currentDisk.add_segment (part.path, part.size, random_color () );
	}

	if (schemes.get_children().length () > 0) {
		// Remove all options from the scheme box
		foreach (unowned Gtk.Widget sprog in schemes.get_children ()) {
			schemes.remove (sprog);
			sprog = null;
		}
	}

	// Now create radio buttons for each item
	var pschemes = PartitionScheme.for_disk (disk);
	var color = Config["Interface"]["Foreground"];
	RadioButton boss = new RadioButton.with_label (null, "<span color=\"%s\">%s</span>".printf(color, pschemes[0].name));
	(boss.child as Gtk.Label).use_markup = true;
	schemes.pack_start (boss, false, false, 10);
	if (pschemes.length > 1) {
		for (int i=1; i<pschemes.length; i++) {
			var radBtn = new Gtk.RadioButton.with_label (boss.get_group (), "<span color=\"%s\">%s</span>".printf(color, pschemes[i].name));
			(radBtn.child as Gtk.Label).use_markup = true;
			schemes.pack_start (radBtn, false, false, 10);
		}
	}
	schemes.show_all ();
   }

   /** Init this page instance */
   public override void init_page () {

	GETTEXT_INIT ();

	short_title = _("Disks");
	title = _("Please select where you wish to install %s").printf (Config["Branding"]["Name"]);

	// We are init'd anytime a show happens
	if (already_init) {
		// Stuff to do before showing
		if (devices == null) {
			devices = DeviceManager.get_all_devices ();
			if (devices.length () >= 1) {
				set_bar_from_disk (devices.nth_data(0).disk);
				foreach (unowned Device d in devices) {
					//Disk d = devices.nth_data(0).disk;
					disk_chooser.add_disk (d.disk);
				}
			}
		}
		navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
		return;
	} else {
		already_init = true;
	}

	display = new Gtk.VBox (false, 10);
	disk_chooser = new DiskChooser ();
	disk_chooser.disk_selected.connect ( (d) => { set_bar_from_disk (d); } );
	display.pack_start (disk_chooser, false, false, 10);

	currentDisk = new SegmentedBar ();

	display.pack_start (currentDisk, false, false, 10);

	// So we can actually display available partitioning schemes
	schemes = new Gtk.VBox (false, 10);
	display.pack_start (schemes, true, true, 50);

	navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
   }

   public override string get_title () { return title; }

   public override string get_short_title () { return short_title; }

   public override Gtk.Widget get_display () { return display; }

   public override string get_section () { return _("Preparation"); }

   /** We are a standard Data Collection page */
   public override PageType get_page_type () { return PageType.DATA; }
}

[ModuleInit]
Type module_init (GLib.TypeModule type_module) {
	message ("Attempt creation of module");
	return typeof (DisksPage);
}

} // End namespace
