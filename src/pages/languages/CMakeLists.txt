
# Build the language page - shared lib (dynamically loaded)
vala_precompile(INSTALLER_LANGUAGE_PAGE
"languages.vala"
PACKAGES
gtk+-2.0
gmodule-2.0
gobject-2.0
gio-2.0
CUSTOM_VAPIS
${CMAKE_SOURCE_DIR}/vapi/config.vapi
${GENERATED_DIR}/configsystem.vapi
${GENERATED_DIR}/il8n.vapi
${GENERATED_DIR}/installerpage.vapi
DIRECTORY
.
OPTIONS
--disable-warnings
-X -DGETTEXT_PACKAGE="solusosinstaller"
)


# Turn WelcomePage into a shared object
add_library ("languagepage" SHARED ${INSTALLER_LANGUAGE_PAGE})

set_target_properties(languagepage PROPERTIES 
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)


# Link welcome page with the config system
target_link_libraries ("languagepage" ${GTK_LIBRARIES})
target_link_libraries ("languagepage" "${INSTALLER_NAME}-il8n")
target_link_libraries ("languagepage" "installerpage")
target_link_libraries ("languagepage" "${INSTALLER_NAME}-config")

INSTALL (PROGRAMS ${CMAKE_BINARY_DIR}/liblanguagepage.so DESTINATION lib/${INSTALLER_NAME})

