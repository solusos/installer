using Gtk;

namespace Installer {

// Used for our ListStore
enum LanguageColumns {
	DESCRIPTION,
	IMAGE,
	CODE,
	MARKUP,
	N_COLUMNS
}

public class LanguagePage : InstallerPage {

   private Gtk.VBox display;
   private string title;
   private string short_title;
   private bool already_init = false;
   private LocaleHelper helper;

   // Our treeview. Represents languages ,etc.
   private TreeView treeview;

   private string selected_locale;
   protected string actual_locale;

   static construct {

	message ("LanguagePage init");
   }

   static ~LanguagePage() {
	message ("LanguagePage destroyed");
   }

   /** Init this page instance */
   public override void init_page () {
	// Ensure gettext loads
	GETTEXT_INIT ();

	// We are init'd anytime a show happens
	if (already_init) {
		if (selected_locale != null) {
			navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
		} else {
			navigate_update (NavigationState.DISABLE, NavigationDirection.FORWARD);
		}
		return;
	} else {
		already_init = true;
	}

	actual_locale = Environment.get_variable ("LANG").split(".")[0];
	display = new Gtk.VBox (false, 0);
	title = _("Please select your language");
	short_title = _("Language");

	// Get a hold of the locale system
	helper = new LocaleHelper ();

	var model = new Gtk.ListStore (LanguageColumns.N_COLUMNS, typeof(string), typeof(Gdk.Pixbuf), typeof(string), typeof(string));

	TreeIter iter;
	foreach (unowned LocaleDef? def in helper.locales) {
		var dispString = "";
		string markup;
		if (def.language == def.language_code ){
			// No name for this language? oO
			dispString = "%s (%s - %s)".printf(def.country, def.country_code, def.language_code);
			markup = "<b>%s</b> - <small>(%s - %s)</small>".printf(def.country, def.country_code, def.language_code);
		} else {
			dispString = "%s (%s)".printf(def.language, def.country);
			markup = "<b>%s</b> - <small>(%s)</small>".printf(def.language, def.country);
		}

		Gdk.Pixbuf pixbuf;
		try {
			string img = "%s/flags/iso/shiny/64/%s.png".printf(RESOURCE_DIR, def.country_code.up());
			pixbuf = new Gdk.Pixbuf.from_file (img);
		} catch (GLib.Error e) {
			pixbuf = null;
			// Load generic ?
		}
		if (pixbuf == null) {
			continue;
		}
		model.append(out iter);
		
		model.set(iter, LanguageColumns.DESCRIPTION, dispString);
		model.set(iter, LanguageColumns.IMAGE, pixbuf);
		model.set(iter, LanguageColumns.CODE, def.locale_code);
		model.set(iter, LanguageColumns.MARKUP, markup);
	}

	// Create our TreeView
	model.set_sort_column_id (0, SortType.ASCENDING);
	treeview = new Gtk.TreeView.with_model (model);
	treeview.headers_visible = false;
	treeview.enable_search = true;
	treeview.enable_grid_lines = TreeViewGridLines.NONE;
	treeview.search_column = LanguageColumns.DESCRIPTION;

	// Hook us up for selection events
	treeview.cursor_changed.connect ( () => {
		TreePath path;
		TreeViewColumn column;
		treeview.get_cursor (out path, out column);
		if (path == null) {
			selected_locale = null;
			navigate_update (NavigationState.DISABLE, NavigationDirection.FORWARD);
			return;
		}
		TreeIter c_iter; // From the TreePath
		model.get_iter (out c_iter, path);

		Value item; // What we're actually interested in :p
		model.get_value (c_iter, LanguageColumns.CODE, out item);

		// Store reference to this purty locale...
		selected_locale = (string)item;

		// Update locale..
		Intl.setlocale(LocaleCategory.MESSAGES, selected_locale);
		message ("Changing locale: %s", selected_locale);

		// Proceed to the next page, we're done here.
		navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
	});

	// Description column
	var col1render = new CellRendererText ();
	var col1 = new TreeViewColumn.with_attributes ("Description", col1render, "markup", LanguageColumns.MARKUP);

	// Pixbuf column
	var col2render = new CellRendererPixbuf ();
	var col2 = new TreeViewColumn.with_attributes ("Image", col2render, "pixbuf", LanguageColumns.IMAGE);

	// Image first, description second.
	treeview.append_column (col2);
	treeview.append_column (col1);

	display = new Gtk.VBox (false, 0);

	var scroller = new Gtk.ScrolledWindow (null, null);
	scroller.hscrollbar_policy = PolicyType.NEVER;
	scroller.add (treeview);
	display.add (scroller);

	// Now just select our in-use locale
	model.foreach((m,p,i) => {
		Value o;
		m.get_value (i, LanguageColumns.CODE, out o);
		string test = o.get_string ();
		if (test == actual_locale) {
			treeview.set_cursor (p, col1, false);
			treeview.scroll_to_cell (p, col1, true, 0.5f, 0.5f);
			return true;
		} else {
			return false;
		}
	});


	display.set_border_width (10);
   }

   public override string get_title () { return title; }

   public override Gtk.Widget get_display () { return display; }

   public override string get_short_title () { return short_title; }

   public override string get_section () { return _("Preparation"); }

   /** We are a standard Data Collection page */
   public override PageType get_page_type () { return PageType.DATA; }

   public override string get_data () { return _("Set locale to %s").printf(selected_locale); }
}

[ModuleInit]
Type module_init (GLib.TypeModule type_module) {
	message ("Attempt creation of module");
	return typeof (LanguagePage);
}

} // End namespace
