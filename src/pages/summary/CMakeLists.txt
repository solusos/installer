
# Build the language page - shared lib (dynamically loaded)
vala_precompile(INSTALLER_SUMMARY_PAGE
"summary.vala"
PACKAGES
gtk+-2.0
gmodule-2.0
gobject-2.0
gio-2.0
CUSTOM_VAPIS
${CMAKE_SOURCE_DIR}/vapi/config.vapi
${GENERATED_DIR}/configsystem.vapi
${GENERATED_DIR}/installerpage.vapi
DIRECTORY
.
OPTIONS
--disable-warnings
-X -DGETTEXT_PACKAGE="solusosinstaller"
)


# Turn WelcomePage into a shared object
add_library ("summarypage" SHARED ${INSTALLER_SUMMARY_PAGE})

set_target_properties(summarypage PROPERTIES 
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)


# Link welcome page with the config system
target_link_libraries ("summarypage" ${GTK_LIBRARIES})
target_link_libraries ("summarypage" "installerpage")
target_link_libraries ("summarypage" "${INSTALLER_NAME}-config")

INSTALL (PROGRAMS ${CMAKE_BINARY_DIR}/libsummarypage.so DESTINATION lib/${INSTALLER_NAME})

