using Gtk;
using Pango;

namespace Installer {

public class SummaryPage : InstallerPage {

   private Gtk.VBox display;
   private string title;
   private string short_title;
   private bool already_init = false;

   private Gtk.TextView textview;
   private Gtk.TextTagTable tags;
   private Gdk.Pixbuf bullet;
   private Gtk.TextBuffer buffer;
   TextIter iter;

   private Gdk.Cursor saved_cursor;

   private int header_index = 0;

   static construct {

	message ("SummaryPage init");
   }

   static ~SummaryPage() {
	message ("SummaryPage destroyed");
   }

   /** Init this page instance */
   public override void init_page () {

	GETTEXT_INIT ();

	// We are init'd anytime a show happens
	if (already_init) {
		// Reset the buffer
		tags = new Gtk.TextTagTable ();
		var BulletPoint = new Gtk.TextTag("bulletpoint");
		tags.add(BulletPoint);
		buffer = new Gtk.TextBuffer (tags);
		buffer.get_start_iter(out iter);
		textview.set_buffer (buffer);

		navigate_update (NavigationState.DISABLE, NavigationDirection.FORWARD);
		return;
	} else {
		already_init = true;
	}

	display = new Gtk.VBox (false, 0);
	title = _("Please review this summary and make sure everything is correct");
	short_title = _("Summary");

	// an instructional message
	var instructions = _("Click any headline to make changes to the options listed below");
	Gtk.Label inst_label = new Gtk.Label ("<span color=\"%s\"><big>%s</big></span>".printf(Config["Interface"]["Foreground"], instructions));
	inst_label.set_alignment (0, 0);
	inst_label.use_markup = true;
	display.pack_start (inst_label, false, false, 20);

	textview = new Gtk.TextView ();
	textview.realize.connect ( () => { 
		unowned Gdk.Window win = textview.get_window (Gtk.TextWindowType.TEXT);
		saved_cursor = win.get_cursor ();
	} );
	textview.motion_notify_event.connect ( (w,e) => {
		int x;
		int y;
		w.window.at_pointer (out x, out y);
		Gtk.TextIter tmp;
		textview.get_iter_at_location (out tmp, x, y);
		var tags = tmp.get_tags ();
		foreach (unowned Gtk.TextTag tag in tags) {
			if (tag.name.has_prefix("hyperlink://")) {
				unowned Gdk.Window win = textview.get_window (Gtk.TextWindowType.TEXT);
				win.set_cursor (new Gdk.Cursor (Gdk.CursorType.HAND2));
				return false;
			}
		}
		unowned Gdk.Window win = textview.get_window (Gtk.TextWindowType.TEXT);
		win.set_cursor (saved_cursor);
		return false;
	} );
	textview.cursor_visible = false;
	textview.editable = false;
	var scrolls = new Gtk.ScrolledWindow (null,null);
	scrolls.vscrollbar_policy = PolicyType.AUTOMATIC;
	scrolls.hscrollbar_policy = PolicyType.NEVER;
	scrolls.add(textview);
	display.pack_start (scrolls, true, true, 10);

	// Sort out TextTagTable
	tags = new Gtk.TextTagTable ();

	// bulletpoint
	bullet = new Gdk.Pixbuf.from_file ("%s/bullet-point.png".printf(RESOURCE_DIR));

	var BulletPoint = new Gtk.TextTag("bulletpoint");
	tags.add(BulletPoint);

	buffer = new Gtk.TextBuffer (tags);

	// Demo stuffs
	buffer.get_start_iter(out iter);

	//add_header ("Sample Header", 0);
	//add_bullet ("Sample item #1");
	//add_bullet ("Sample item #2");
	//add_bullet ("Sample item #3");

	//add_header ("Sample Header #2", 1, true);

	textview.set_buffer (buffer);
   }

   /**
    * Add a header to the buffer 
    */
   private void add_header (string header, int index, DataCallback cb, bool space = false) {
	if (space) {
		var spacing = "\n";
		buffer.insert (iter, spacing, (int)spacing.length);
	}

	var HyperLink = buffer.create_tag("hyperlink://%d".printf(index));
	HyperLink.event.connect ( (o,e,i) => {
		if (e.type == Gdk.EventType.BUTTON_PRESS) {
			if (e.button.button != 1) return false;
			Idle.add ( () => { cb (index); return false; } );
		}
		return false;
	} );
	HyperLink.foreground = "#0000FF";
	HyperLink.size_points = 16;
	HyperLink.indent = 3;
	HyperLink.underline = Pango.Underline.SINGLE;

	buffer.insert_with_tags (iter, header, (int)header.length, HyperLink);

	// Add two new blank lines
	var lines = "\n\n";
	buffer.insert (iter, lines, (int)lines.length);
   }

   /**
    * Add a bullet-point to the buffer
    */
   private void add_bullet (string bullet_text) {
	var part2 = bullet_text + "\n";
	var spacing = "\t";
	buffer.insert_with_tags_by_name (iter, spacing, (int)spacing.length, "bulletpoint");
	buffer.insert_pixbuf (iter, bullet);
	buffer.insert_with_tags_by_name (iter, part2, (int)part2.length, "bulletpoint");
   }

   public override string get_title () { return title; }

   public override string get_short_title () { return short_title; }

   public override Gtk.Widget get_display () { return display; }

   public override string get_section () { return _("Preparation"); }

   /** We are the summary page */
   public override PageType get_page_type () { return PageType.SUMMARY; }

   /**
    * Handle data :)
    */
   public override void process_data (string source, string? data, DataCallback cb, int index) {
	add_header (source, index, cb, true);
	if (data != null) {
		add_bullet (data);
	} else {
		add_bullet ("This module currently provides no data");
	}
   }
}

[ModuleInit]
Type module_init (GLib.TypeModule type_module) {
	message ("Attempt creation of module");
	return typeof (SummaryPage);
}

} // End namespace
