using Gtk;
using Installer.Widgets;

namespace Installer {

public class TimezonePage : InstallerPage {

   private Gtk.VBox display;
   private string title;
   private string short_title;
   private bool already_init = false;

   // Our map
   private TimezoneMap map;
   private LocationDatabase db;

   private string zone;

   // Original zone, for reset
   private string zone_orig;

   static construct {

	message ("TimezonePage init");
   }

   static ~TimezonePage() {
	message ("TimezonePage destroyed");
   }

   /** Init this page instance */
   public override void init_page () {

	GETTEXT_INIT ();

	// We are init'd anytime a show happens
	if (already_init) {
		// Need to do this in a cleaner non-blocking way
		if (zone == null) {
			//var location = db.get_current_country ();
			db.get_current_country.begin ( (s,res) => {
				var location = ((LocationDatabase)s).get_current_country.end(res);
				if (location != null) {
					map.select_country (location);
					this.zone = location;
					this.zone_orig = location;

				}
				progress_update (ProgressState.HIDE, "", -1);
			} );
			progress_update (ProgressState.SHOW, _("Attempting to determine location via IP address..."), -1);
		}
		navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
		return;
	} else {
		already_init = true;
	}

	display = new Gtk.VBox (false, 0);
	title = _("Please select your timezone");
	short_title = _("Timezone");

	db = new LocationDatabase ();

	map = new TimezoneMap ("%s/timezone".printf(RESOURCE_DIR), db);
	map.city_selected.connect(this.city_selected);

	display.pack_start (map, true, true, 60);

	// Box.. for the reset button
	var btn = new Gtk.Button.with_label ( _("Reset location") );
	btn.clicked.connect ( () => {
		if (zone_orig != null) { 
			map.select_country (zone_orig);
		}
	} );

	var holder = new Gtk.HButtonBox ();
	holder.add (btn);
	display.pack_end (holder, false, false, 5);

	navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
   }

   protected void city_selected(string zone) {
	this.zone = zone;

	if (this.zone != null) {
		navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
	} else {
		navigate_update (NavigationState.DISABLE, NavigationDirection.FORWARD);
	}
   }

   public override string get_title () { return title; }

   public override string get_short_title () { return short_title; }

   public override Gtk.Widget get_display () { return display; }

   public override string get_section () { return _("Preparation"); }

   /** We are a standard Data Collection page */
   public override PageType get_page_type () { return PageType.DATA; }

   public override string get_data () { return _("Set timezone to %s").printf(this.zone); }
}

[ModuleInit]
Type module_init (GLib.TypeModule type_module) {
	message ("Attempt creation of module");
	return typeof (TimezonePage);
}

} // End namespace
