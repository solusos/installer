using Gtk;

namespace Installer {

public class WelcomePage : InstallerPage {

   private Gtk.Label display;
   private string title;
   private string short_title;
   private bool already_init = false;

   static construct {

	message ("WelcomePage init");
   }

   static ~WelcomePage() {
	message ("WelcomePage destroyed");
   }

   /** Init this page instance */
   public override void init_page () {

	GETTEXT_INIT ();

	// We are init'd anytime a show happens
	if (already_init) {
		navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
		return;
	} else {
		already_init = true;
	}
	string distName;
	getConfig().get_string("Branding", "Name", out distName);

	display = new Gtk.Label("");
	title = _("Welcome to SolusOS 1.3");
	short_title = _("Welcome");

	bool use_markup;
	getConfig().get_bool("ReleaseNotes", "Use-Markup", out use_markup);
	display.use_markup = use_markup;
	var markup = new StringBuilder ();
	string rel_notes_file;
	getConfig().get_string("ReleaseNotes", "URL", out rel_notes_file);
	var path = "%s/%s".printf(RESOURCE_DIR, rel_notes_file);
	File notes = File.new_for_path (path);
	try {
		string line;
		var dis = new DataInputStream (notes.read(null));
		while ( (line = dis.read_line (null,null)) != null) {
			markup.append(line + "\n");
		}
	} catch (Error e) {
		// ignore for now
		error (e.message);
	}
	display.label = markup.str;

	navigate_update (NavigationState.ENABLE, NavigationDirection.FORWARD);
   }

   public override string get_title () { return title; }

   public override string get_short_title () { return short_title; }

   public override Gtk.Widget get_display () { return display; }

   public override string get_section () { return _("Preparation"); }

   /** We are a presentation page (no data) */
   public override PageType get_page_type () { return PageType.PRESENTATION; }
}

[ModuleInit]
Type module_init (GLib.TypeModule type_module) {
	message ("Attempt creation of module");
	return typeof (WelcomePage);
}

} // End namespace
