using Gtk;
using Cairo;
using Gdk;

namespace Installer {

private static ResourceManager currentRM;
public static ResourceManager rm() { 
	if (currentRM == null) {
			currentRM = new ResourceManager ();
	}
	return currentRM;
}

/**
 * Utility method, return an image from the resource manager
 */
public static void get_image (string path, out Gtk.Image image) {
	var img = rm().get_image(path);
	image = new Gtk.Image.from_pixbuf (img);
}

/**
 * Utility method, return a pixbuf from the resource manager
 */
public static void get_pixbuf (string path, out Gdk.Pixbuf image) {
	image = rm().get_image(path);
}
public class ResourceManager {

   public Gdk.Pixbuf get_image(string path) {
	return new Gdk.Pixbuf.from_file ("%s/%s".printf(RESOURCE_DIR, path));
   }
}

} // End namespace
