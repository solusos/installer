SolusOS Installer UI Widgets
----------------------------
Any custom UI widget for the SolusOS Installer should be stored in this directory

Current Widgets
---------------
 * TickerBox - simple on/off display widget with label (non-interactive)
 * TimezoneMap - Timezone selection via graphical interactive map
 * SegmentedBar - Disk usage GUI representation via a bar
