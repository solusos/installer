using Gtk;
using Installer;

namespace Installer.Widgets {

public class DiskItem : Gtk.HBox {

   public Disk disk { private set; public get; }
   private Gtk.Image image;
   private Gtk.Label label;

   /**
    * Construct a new DiskItem using the Installer.Disk object
    */
   public DiskItem (Disk disk) {
	this.disk = disk;
	var itheme = IconTheme.get_default ();
	Gdk.Pixbuf icon;
	try {
		icon = itheme.load_icon ("drive-harddisk-scsi", 64, 0);
		image = new Gtk.Image.from_pixbuf (icon);
	} catch (Error e) { warning (e.message); }

	var fgColor = Config["Interface"]["Foreground"];

	this.pack_start (image, false, false, 10);
	//var disk_info = "<big>%s (%s)</big>\n<small>%s</small>".printf(disk.model, misc.format_size(disk.size), disk.type);
	var disk_info = "<big>%s (%s)</big>\n<small>%s</small>".printf (disk.model, format_size (disk.size), disk.disk_type );

	var disp_label = "<span color=\"%s\">%s</span>".printf(fgColor, disk_info);
	// Make sure user knows its SSD
	if (disk.ssd) {
		disp_label = "<span color=\"%s\">%s (SSD)</span>".printf(fgColor, disk_info);
	}




	label = new Gtk.Label (disp_label);
	label.use_markup = true;

	this.pack_start (label, false, false, 0);
   }

/**
 * Utility method. Return a string representation of disk size (human readable)
 * NEEDS TO BE MOVED TO CORE - REPLICATION OF CODE
 */
   private string format_size (uint64 size) {
	string unit;
	uint64 factor;
	if (size < 1000) {
		unit = "B";
		factor = 1;
	} else if (size < 1000*1000) {
		unit = "kB";
		factor = 1000;
	} else if (size < 1000 * 1000 * 1000) {
		unit = "MB";
		factor = 1000 * 1000;
	} else if (size < 1000 * 1000 * 1000 * 1000) {
		unit = "GB";
		factor = 1000 * 1000 * 1000;
	} else {
		unit = "TB";
		factor = 1000 * 1000 * 1000 * 1000;
	}

	return "%.1f %s".printf ((float)(size / factor), unit);
   }
} // End DiskItem definition

public class DiskChooser : Gtk.HBox {

   private Gtk.HBox holder;
   private Gtk.Button backBtn;
   private Gtk.Button forwardBtn;

   private List<Disk> disks;

   private weak Disk current_disk;

   private DiskItem current_disk_item;

   private int index = 0;

   /**
    * Construct a new DiskChooser
    */
   public DiskChooser () {
	// holding area
	holder = new Gtk.HBox (false, 0);
	this.pack_start (holder, true, true, 0);

	// Create our button box
	var btnBox = new Gtk.HBox (false, 0);
	btnBox.set_border_width (20);

	// Back button
	backBtn = new Gtk.Button ();
	backBtn.sensitive = false;
	var backArrow = new Gtk.Arrow (ArrowType.LEFT, ShadowType.NONE);
	backBtn.add(backArrow);
	backBtn.clicked.connect ( () => navigate (false) );
	btnBox.pack_start (backBtn, false, false, 0);

	// Forward button
	forwardBtn = new Gtk.Button ();
	forwardBtn.sensitive = false;
	var forwardArrow = new Gtk.Arrow (ArrowType.RIGHT, ShadowType.NONE);
	forwardBtn.add(forwardArrow);
	forwardBtn.clicked.connect ( () => navigate (true) );
	btnBox.pack_start (forwardBtn, false, false, 0);

	this.pack_end (btnBox, false, false, 0);

	disks = new List<Disk> ();

	this.show_all ();
   }

   protected void navigate (bool forward = true) {
	if (forward) {
		index += 1;
	} else {
		index -= 1;
	}
	check_limits ();
	this.holder.remove (current_disk_item);
	current_disk = disks.nth_data (index);
	current_disk_item = new DiskItem(current_disk);
	holder.add (current_disk_item);
	holder.show_all ();

	disk_selected (current_disk);
   }

   private void check_limits () {
	if (index >= disks.length() - 1) {
		forwardBtn.sensitive = false;
		index = (int)disks.length() - 1;
	} else {
		forwardBtn.sensitive = true;
	}

	if (index <= 0) {
		index = 0;
		backBtn.sensitive = false;
	} else {
		backBtn.sensitive = true;
	}
   }

   /**
    * Add a disk to this chooser
    */
   public void add_disk (Disk newDisk) {
	disks.append (newDisk);
	if (current_disk == null) {
		current_disk = newDisk;
		current_disk_item = new DiskItem (newDisk);
		holder.add (current_disk_item);
		holder.show_all ();
	} else {
		forwardBtn.set_sensitive (true);
	}
   }

   // Emitted when a disk is selected in the UI
   public signal void disk_selected (Disk disk);
}


} // End namespace
