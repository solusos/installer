using Gtk;
using Cairo;
using Gdk;
using Math;

namespace Installer.Widgets {

/**
 * Used as a flag
 */
public struct CairoCorners {
   static uint NO_CORNERS = 0;
   static uint TOP_LEFT = 1;
   static uint TOP_RIGHT = 2;
   static uint BOTTOM_LEFT = 4;
   static uint BOTTOM_RIGHT = 8;
   static uint ALL = 15;
}

/**
 * Represent colors in cairo
 */
public class Color {

   public double r { public set; public get; }
   public double g { public set; public get; }
   public double b { public set; public get; }
   public double a { public set; public get; }

   public Color (double red, double green, double blue, double alpha=1.0) {
	this.r = red;
	this.g = green;
	this.b = blue;
	this.a = alpha;
   }
} // End Color class definition

/**
 * CairoExtensions, utility methods for Cairo using apps
 */
public class CairoExtensions {

   /**
    * Utility method, modula
    */
   public static int modula(double number, double divisor) {
	return (int ) ((number % divisor) + (number - (int)number)) ;
   }

   /**
    * Convert a Gdk.Color to our internal (cairo) color system
    */
   public static Installer.Widgets.Color gdk_color_to_cairo_color (Gdk.Color color, float alpha=1.0f) {
	return new Installer.Widgets.Color ((color.red >> 8) / 255.0,
            (color.green >> 8) / 255.0,
            (color.blue >> 8) / 255.0,
            alpha);
   }

   /**
    * Convert HSB values into a Color object
    */
   public static Installer.Widgets.Color color_from_hsb(double hue, double saturation, double brightness) {
	double[] hue_shift = new double[] { 0, 0, 0};
	double[] color_shift = new double[] { 0, 0, 0};

	double m2;

	if (brightness <= 0.5) {
		m2 = brightness * (1 + saturation);
	} else {
		m2 = brightness + saturation - brightness * saturation;
	}

	double m1 = 2 * brightness - m2;
        hue_shift[0] = hue + 120;
        hue_shift[1] = hue;
        hue_shift[2] = hue - 120;
        color_shift[0] = brightness;
        color_shift[1] = brightness;
        color_shift[2] = brightness;

	int i = 0;
	if (saturation == 0) {
		i = 3;
	}

	while (i < 3) {
		double m3 = hue_shift [i];
		if (m3 > 360) {
			m3 = CairoExtensions.modula (m3, 360);
		} else if (m3 <0) {
			m3 = 360 - CairoExtensions.modula (Math.fabs(m3), 360);
		}

		if (m3 < 60) {
			color_shift[i] = m1 + (m2 - m1) * m3 / 60;
		} else if (m3 < 180) {
			color_shift[i] = m2;
		} else if (m3 < 240) {
			color_shift[i] = m1 + (m2 - m1) * (240 - m3) / 60;
		} else {
			color_shift[i] = m1;
		}

		i += 1;
	}
	return new Color (color_shift[0], color_shift[1], color_shift[2]);
   }

   /**
    * Return HSB for Color object
    */
   public static double[] hsb_from_color (Installer.Widgets.Color color) {

	double[] ret = new double[3];

	double red = color.r;
	double green = color.g;
	double blue = color.b;
	double hue = 0;
	double saturation = 0;
	double brightness = 0;

	double ma;
	double mi;

	double max = Math.fmax ( Math.fmax (red, green), blue);

	if (red > green) {
		ma = Math.fmax (red, green);
		mi = Math.fmax (green, blue);
	} else {
		ma = Math.fmax (green, blue);
		mi = Math.fmax (red, blue);
	}

	brightness = (ma + mi) / 2;

	if ( Math.fabs (ma - mi) < 0.0001) {
		hue = 0;
		saturation = 0;
	} else {

		if (brightness <= 0.5) {
			saturation = (ma - mi) / (ma + mi);
		} else {
			saturation = (ma - mi) / (2 - ma - mi);
		}

		double delta = ma - mi;
		if (red == max) {
			hue = (green - blue) / delta;
		} else if (green == max) {
			hue = 2 + (blue - red) / delta;
		} else if (blue == max) {
			hue = 4 + ( red - green) / delta;
		}
		hue *= 60;
		if (hue < 0) {
			hue += 360;
		}
	}
	ret[0] = hue;
	ret[1] = saturation;
	ret[2] = brightness;

	return ret;
   }

   /**
    * Convert rgba value to Color
    */
   public static Installer.Widgets.Color rgba_to_color (int64 color) {
	double a = ((color >> 24) & 0xff) / 255.0;
        double b = ((color >> 16) & 0xff) / 255.0;
        double c = ((color >> 8) & 0xff) / 255.0;
        double d = (color & 0x000000ff) / 255.0;

	return new Installer.Widgets.Color (a, b, c, d);
   }

   /** Convert hexval character to int */
   public static int hexval( string c ) {
	switch(c) {
		case "a":
			return 10;
		case "b":
			return 11;
		case "c":
			return 12;
		case "d":
			return 13;
		case "e":
			return 14;
		case "f":
			return 15;
		default:
			return c.to_int();
	}
   }

   /** Convert hex string to int */
   public static int64 hextoint(string hex){
	//convert the string to lowercase
	string hexdown = hex.down();
	//get the length of the hex string
	int hexlen = (int)hex.length;
	int64 ret_val = 0;
	string chr;
	int chr_int;
	int multiplier;

	//loop through the string 
	for (int i = 0; i < hexlen ; i++) {
		//get the string chars from right to left
		int inv = (hexlen-1)-i;
		chr = hexdown[inv:inv+1];
		chr_int = hexval(chr);

		//how are we going to multiply the current characters value?
		multiplier = 1;
		for(int j = 0 ; j < i ; j++) {
			multiplier *= 16;
		}
		ret_val += chr_int * multiplier;
	}
	return ret_val;
   }

   /**
    * Convert RGB string to Color
    */
   public static Installer.Widgets.Color rgb_to_color (string color) {
	string red = color.substring (0, 2);
	string green = color.substring (2, 4);
	string blue = color.substring (4);

	double r = (double) hextoint (red);
	double g = (double) hextoint (green);
	double b = (double) hextoint (blue);

	return new Color (r, g, b);
   }

   public static Installer.Widgets.Color color_shade (Installer.Widgets.Color cin, double ratio) {
	Color c;
	double [] hsb = CairoExtensions.hsb_from_color (cin);
	var h = hsb[0];
	var s = hsb[1];
	var b = hsb[2];
        b = fmax(fmin(b * ratio, 1), 0);
        s = fmax(fmin(s * ratio, 1), 0);
        c = CairoExtensions.color_from_hsb(h, s, b);
        c.a = cin.a;
        return c;
   }

   /**
    * Draw a rounded rectangle
    */
   public static void rounded_rectangle (Cairo.Context cr, double x, double y, double w, double h, double r, uint corners = CairoCorners.ALL, bool top_bottom_falls_through = false) {
	if (top_bottom_falls_through && corners == CairoCorners.NO_CORNERS) {
		cr.move_to (x, y -r);
		cr.line_to (x, y + h + r);
		cr.move_to (x + w, y - r);
		cr.line_to (x + w, y + h + r);
	} else if (r < 0.0001 || corners == CairoCorners.NO_CORNERS) {
		cr.rectangle (x, y, w, h);
	}

	if ( (corners & (CairoCorners.TOP_LEFT | CairoCorners.TOP_RIGHT) ) == 0 && top_bottom_falls_through) {
 		y = y - r;
		h = h + r;
		cr.move_to (x + w, y);
	} else {
		if ( (corners & CairoCorners.TOP_LEFT) != 0) {
			cr.move_to (x + r, y);
		} else {
			cr.move_to (x, y);
		}

		if ( (corners & CairoCorners.TOP_RIGHT) != 0) {
			cr.arc (x + w -r, y + r, r, PI * 1.5, PI * 2);
		} else {
			cr.line_to (x + w, y);
		}
	}

	if ( (corners & (CairoCorners.BOTTOM_LEFT | CairoCorners.BOTTOM_RIGHT)) == 0 && top_bottom_falls_through) {
		h = h + r;
		cr.line_to (x + w, y + h);
		cr.move_to (x, y + h);
		cr.line_to (x, y + r);
		cr.arc (x + r, y + r, r, PI, PI * 1.5);
	} else {
		if ( (corners & CairoCorners.BOTTOM_RIGHT) != 0) {
			cr.arc (x + w - r, y + h - r, r, 0, PI * 0.5);
		} else {
			cr.line_to (x + w, y + h);
		}

		if ( (corners & CairoCorners.BOTTOM_LEFT) != 0) {
			cr.arc (x + r, y + h - r, r, Math.PI * 0.5, Math.PI);
		} else {
			cr.line_to (x, y + h);
		}

		if ( (corners & CairoCorners.TOP_LEFT) != 0) {
			cr.arc (x + r, y + r, r, PI, PI * 1.5);
		} else {
			cr.line_to (x, y);
		}


	
	}
   }
} // End CairoExtensions class definition
} // End namespace
