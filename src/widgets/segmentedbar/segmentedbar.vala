using Gtk;
using Pango;
using Math;

namespace Installer.Widgets {

/**
 * Utility method. Return a string representation of disk size (human readable)
 */
public static string format_size (uint64 size) {
	string unit;
	uint64 factor;
	if (size < 1000) {
		unit = "B";
		factor = 1;
	} else if (size < 1000*1000) {
		unit = "kB";
		factor = 1000;
	} else if (size < 1000 * 1000 * 1000) {
		unit = "MB";
		factor = 1000 * 1000;
	} else if (size < 1000 * 1000 * 1000 * 1000) {
		unit = "GB";
		factor = 1000 * 1000 * 1000;
	} else {
		unit = "TB";
		factor = 1000 * 1000 * 1000 * 1000;
	}

	return "%.1f %s".printf ((float)(size / factor), unit);
}

public class SegmentedBar : Gtk.EventBox {

   // Our segments
   private List<Segment> segments;

   // Our size
   private double layout_width = 0;
   private double layout_height = 0;

   // Height of the bar
   public int bar_height  { public set; public get; } 

   // Vertical space between the bar and label
   public int bar_label_spacing  { public set; public get; } 

   // Horizontal space between the label and the next box
   public int segment_label_spacing { public set; public get; }

   // Size of each segment
   public double segment_box_size { public set; public get; }

   // Spacing for segment boxes
   public int segment_box_spacing { public set; public get; }

   // Padding
   public int h_padding { public set; public get; }

   // Center labels ?
   public bool center_labels { public set; public get; }

   // Show labels ?
   public bool show_labels { public set; public get; }

   // Draw reflections ?
   public bool reflect { public set; public get; }

   // Color to draw remaining space in
   public string remainder_color { public set; public get; }

   // Total size of the disk
   public uint64 disk_size { public set; public get; }

   // Normal pango size
   private int pango_size_normal = 0;

   public SegmentedBar () {
	// Defaults
	bar_height = 30;
	bar_label_spacing = 9;
	segment_label_spacing = 116;
	segment_box_size = 12;
	segment_box_spacing = 6;
	h_padding = 0;
	center_labels = false;
	show_labels = true;
	reflect = true;
	remainder_color = "eeeeee";
	disk_size = 0;

	// We don't need to draw the eventbox's input window, hide it.
	set_visible_window (false);

	segments = new List<Segment> ();
   }

   /**
    * Add a segment to the bar
    */
   public void add_segment (string title, uint64 size, Installer.Widgets.Color color, bool show_in_bar = true) {
	// do_size_allocate (allocation)
	disk_size += size;
	Segment s = new Segment (title, size, color, show_in_bar);
	segments.append (s);
	queue_draw ();
   }

   /**
    * Add a segment using RGB notation
    */
   public void add_segment_rgb (string title, uint64 size, string rgb_color) {
	add_segment (title, size, CairoExtensions.rgb_to_color (rgb_color));
   }

   /**
    * Remove all segments
    */
   public void remove_all () {
	segments = null;
	segments = new List<Segment> ();
   }

   /**
    * Utility method, create pango layout
    */
   protected Pango.Layout create_adapt_layout (Pango.Layout layout, bool small, bool bold) {
	FontDescription fd;
	Pango.Layout layout2 = layout;
	if (layout == null) {
		var context = create_pango_context();
		layout2 = new Pango.Layout (context);
		fd = layout.get_context().get_font_description ();
		pango_size_normal = fd.get_size ();
	} else {
		fd = layout.get_context().get_font_description();
	}

        if (small) {
		fd.set_size( (int) (fd.get_size() * Pango.Scale.SMALL));
        } else {
		fd.set_size(pango_size_normal);
	}

        if (bold) {
		fd.set_weight(Pango.Weight.BOLD);
	} else {
		fd.set_weight(Pango.Weight.NORMAL);
	}
        layout2.context_changed();
        return layout2;
   }

   /**
    * Compute the layout
    */
   public void compute_layout_size () {
	Pango.Layout layout = null;
	layout_height = 0;
	layout_width = 0;

	for (int i=0; i <= segments.length(); i++) {
		Segment segment = segments.nth_data(i);
		layout = create_adapt_layout (layout, false, true);
		var title = segment.title;
		layout.set_text (title, -1); // May need to fix in future
		int aw;
		int ah;
		layout.get_pixel_size (out aw, out ah);

		layout = create_adapt_layout(layout, true, false);
		layout.set_text (segment.subtitle, -1);
		int bh;
		int bw;
		layout.get_pixel_size (out bw, out bh);

		var w = Math.fmax(aw,bw);
		var h = ah + bw;

		segment.layout_width = w;
		segment.layout_height = Math.fmax (h, segment_box_size * 2);

		if ( i < (segments.length() - 1)) {
			layout_width = layout_width + segment.layout_width + segment_box_size + segment_box_spacing + segment_label_spacing;
		} else {
			layout_width = layout_width + segment.layout_width + segment_box_size + segment_box_spacing + 0;
		}
		layout_height = Math.fmax (layout_height, segment.layout_height);
	}
   }

   /**
    * Render the segments
    */
   protected void render_bar_segments (Cairo.Context cr, double w, double h, double r) {
	var grad = new Cairo.Pattern.linear (0, 0, w, 0);
	var last = 0.0;

	foreach (Segment segment in segments) {
		var percent = segment.size /  (float)disk_size;
		if (percent > 0) {
			grad.add_color_stop_rgb(last, segment.color.r, segment.color.g, segment.color.b);
			last = last + percent;
			grad.add_color_stop_rgb(last, segment.color.r, segment.color.g, segment.color.b);
		}
	}

	// Draw the "filled in" part
	CairoExtensions.rounded_rectangle (cr, 0, 0, w, h, r, CairoCorners.NO_CORNERS);
	cr.set_source (grad);
	cr.fill_preserve ();

	var grad2 = new Cairo.Pattern.linear (0, 0, 0, h);
	grad.add_color_stop_rgba (0.0, 1, 1, 1, 0.125);
	grad.add_color_stop_rgba (0.35, 1, 1, 1, 0.255);
	grad.add_color_stop_rgba (1, 0, 0, 0, 0.4);

	cr.set_source (grad2);
	cr.fill ();
   }

   /**
    * Make a gradient for the Segment
    */
   protected Cairo.Pattern make_segment_gradient (double h, Installer.Widgets.Color color) {
	var grad = new Cairo.Pattern.linear (0, 0, 0, h);
        var c = CairoExtensions.color_shade(color, 1.1);
        grad.add_color_stop_rgba(0.0, c.r, c.g, c.b, c.a);
        c = CairoExtensions.color_shade(color, 1.2);
        grad.add_color_stop_rgba(0.35, c.r, c.g, c.b, c.a);
        c = CairoExtensions.color_shade(color, 0.8);
        grad.add_color_stop_rgba(1, c.r, c.g, c.b, c.a);
        return grad;
   }

   /**
    * Render bar strokes.
    */
   protected void render_bar_strokes (Cairo.Context cr, double w, double h, double r) {
	var stroke = make_segment_gradient(h, CairoExtensions.rgba_to_color(0x00000040));
	var seg_sep_light = make_segment_gradient(h, CairoExtensions.rgba_to_color(0xffffff20));
	var seg_sep_dark = make_segment_gradient(h, CairoExtensions.rgba_to_color(0x00000020));

	cr.set_line_width(1);
	var seg_w = 20;

	double x;
        if (seg_w > r) {
            x = seg_w;
        } else {
            x = r;
	}
        while (x <= w - r) {
            cr.move_to(x - 0.5, 1);
            cr.line_to(x - 0.5, h - 1);
            cr.set_source(seg_sep_light);
            cr.stroke();

            cr.move_to(x + 0.5, 1);
            cr.line_to(x + 0.5, h - 1);
            cr.set_source(seg_sep_dark);
            cr.stroke();
            x = x + seg_w;
	}


        cr.set_source(stroke);
        CairoExtensions.rounded_rectangle(cr, 0.5, 0.5, w - 1, h - 1, r, CairoCorners.NO_CORNERS); // Totally wrong angle :P
        cr.stroke();
	// This draws the outside of the bar itself - THIS IS THE BROKEN METHOD...

   }

   /**
    * Render the labels
    */
    protected void render_labels (Cairo.Context cr) {
	if (segments.length() == 0) {
		return;
	}
        //text_color = CairoExtensions.gdk_color_to_cairo_color(get_style().fg[state])
	Gdk.Color parsage;
	Gdk.Color.parse(Config["Interface"]["Foreground"], out parsage);
	var text_color = CairoExtensions.gdk_color_to_cairo_color (parsage);
        var box_stroke_color = new Color(0, 0, 0, 0.6);
        double x = 0;
        Pango.Layout  layout = null;

	Pango.Context ct = create_pango_context ();
	layout = new Pango.Layout (ct);
	foreach (Segment segment in segments) {
		cr.set_line_width(1);
		//cr.rectangle(x + 0.5, 2 + 0.5, segment_box_size - 1, segment_box_size - 1);
		// Draw a rounded legend key
		CairoExtensions.rounded_rectangle(cr, x + 0.5, 2 + 0.5, segment_box_size - 1, segment_box_size - 1, 3);
		var grad = make_segment_gradient(segment_box_size, segment.color);
		cr.set_source(grad);
		cr.fill_preserve();
		cr.set_source_rgba(box_stroke_color.r, box_stroke_color.g, box_stroke_color.b, box_stroke_color.a);
		cr.stroke();

		x += segment_box_size + segment_box_spacing;

		layout = create_adapt_layout(layout, false, true);
		layout.set_text(segment.title, -1);

		int lw;
		int lh;
		layout.get_pixel_size (out lw, out lh);

		cr.move_to(x, 0);
		text_color.a = 0.9;
		cr.set_source_rgba(text_color.r, text_color.g, text_color.b, text_color.a);
		var pc = Pango.cairo_create_context(cr);
		Pango.cairo_show_layout (cr, layout);
		cr.fill();

		layout = create_adapt_layout(layout, true, false);
		layout.set_text(segment.subtitle, -1);

		cr.move_to(x, lh);
		text_color.a = 0.75;
		cr.set_source_rgba(text_color.r, text_color.g, text_color.b, text_color.a);
		pc = Pango.cairo_create_context(cr);
		Pango.cairo_show_layout (cr, layout);
		cr.fill();
		x = x + segment.layout_width + segment_label_spacing;
	}
   }

   /**
    * Render the bar itself
    */
   protected Cairo.Pattern render_bar (int w, int h) {
	var s = new Cairo.ImageSurface (Cairo.Format.ARGB32, w, h);
	var cr = new Cairo.Context(s);
	render_bar_segments (cr, w, h, h / 2);
	render_bar_strokes (cr, w, h, h / 2);

	var pattern = new Cairo.Pattern.for_surface (s);

	return pattern;
   }

   /**
    * Actually draw the bar
    */
   public override bool expose_event (Gdk.EventExpose e) {
        var cr = Gdk.cairo_create( window);
        if (reflect) {
		cr.push_group();
	}
	cr.set_operator(Cairo.Operator.OVER);
	cr.translate(allocation.x + h_padding, allocation.y);
	cr.rectangle(0, 0, allocation.width - h_padding,
	    Math.fmax(2 * bar_height,
	    bar_height + bar_label_spacing + layout_height));
	cr.clip();

	var bar = render_bar(allocation.width - 2 * h_padding,
	    bar_height);

	cr.save();
	cr.set_source(bar);
	cr.paint();
	cr.restore();

	if (reflect) {
		cr.save();
		cr.rectangle(0, bar_height, allocation.width - h_padding, bar_height);
		cr.clip();
		var matrix = new Cairo.Matrix(1, 0, 0, -1, 0, 0);
		matrix.translate(0, -(2 * bar_height) + 1);
		cr.transform(matrix);
		cr.set_source(bar);

		var  mask = new Cairo.Pattern.linear(0, 0, 0, bar_height);
		var c = new Color(0, 0, 0, 0);
		mask.add_color_stop_rgba(0.25, c.r, c.g, c.b, c.a);
		c = new Color(0, 0, 0, 0.125);
		mask.add_color_stop_rgba(0.5, c.r, c.g, c.b, c.a);
		c = new Color(0, 0, 0, 0.4);
		mask.add_color_stop_rgba(0.75, c.r, c.g, c.b, c.a);
		c = new Color(0, 0, 0, 0.7);
		mask.add_color_stop_rgba(1.0, c.r, c.g, c.b, c.a);

		cr.mask(mask);
		cr.restore();
		cr.pop_group_to_source();
		cr.paint();
	}
	if (show_labels) {
		if (reflect) {
			if (center_labels) {
				cr.translate(allocation.x + (allocation.width -
					 layout_width) / 2, allocation.y +
					 bar_height + bar_label_spacing);
               		} else {
				cr.translate(allocation.x + h_padding,
					 allocation.y + bar_height +
					 bar_label_spacing);
			}
		} else {
			cr.translate(-h_padding + (allocation.width - 
		 	   layout_width) / 2, bar_height + 
		   	 bar_label_spacing);
		}
		render_labels(cr);
	}
        return true;
   }

} // End SegmentedBar class definition

/**
 * Represents a disk slice
 */
public class Segment : GLib.Object {

   public string device { public get; public set; }
   public string title { public get; public set; }
   public string subtitle { public get; public set; }

   public double layout_width { public get; public set; }
   public double layout_height { public get; public set; }


   public uint64 size = 0; 
   public bool show_in_bar { public get; public set; }

   public Installer.Widgets.Color color { public set; public get; }

   /**
    * Create a new Segment object
    */
   public Segment (string device, uint64 size, Installer.Widgets.Color color, bool show_in_bar = true) {
	this.device = device;
	this.title = "";

	// TODO: Set title from os prober
	// Example Python code:
        //    if device.startswith('/'):
        //        title = find_in_os_prober(device)
        //    if title:
        //        title = '%s (%s)' % (title, device)
        //    else:
        //       title = device
	this.title = device;
	set_size (size);
	this.show_in_bar = show_in_bar;
	this.color = color;

	layout_width = 0;
	layout_height = 0;
   }

   /**
    * Set a subtitle with size formatted
    */
   public void set_size (uint64 size) {
	this.size = size;
	if (size > 0) {
		subtitle = format_size (size);
	} else {
		subtitle = "";
	}
   }


} // End Segment class definiton
} // End namespace

