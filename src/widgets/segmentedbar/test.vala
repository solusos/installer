using Gtk;
using Installer.Widgets;

public class WidgetTest {

   private Gtk.Window window;
   private SegmentedBar bar;

   public WidgetTest () {
	window = new Gtk.Window ();
	window.title = "WidgetTest - SegmentedBar";
	window.set_size_request (400, 400);

	window.set_border_width(20);

	bar = new SegmentedBar ();
	bar.add_segment ( "/dev/sda1", (1000*1000*1000) / 2,  new Color (0.5, 0.5, 0.5, 1.0));
	bar.add_segment_rgb ( "/dev/sda2", (1000*1000 * 1000) / 2, "FFFFFF");

	bar.disk_size =  (1000 * 1000 * 1000);
	var box = new Gtk.VBox (false, 0);
	box.add (bar);

	window.add(box);

	window.destroy.connect (Gtk.main_quit);

	window.show_all ();
   }
}

public static void main(string[] args) {
 	Gtk.init (ref args);
	WidgetTest wt = new WidgetTest ();

	Gtk.main ();
}

