using Gtk;
using Gdk;

namespace Installer.Widgets {

public class TickerBox : Gtk.HBox {

   // Not yet completed
   Gdk.Pixbuf off;

   // Completed
   Gdk.Pixbuf on;

   // Our image widget.
   Gtk.Image image;

   // Label to display
   Gtk.Label label;

   // Associated page number
   public uint page_number { public set; public get; }

   /**
    * Construct a new TickerBox
    */
   public TickerBox () {
	get_pixbuf("ticked.png", out on);
	get_pixbuf("unticked.png", out off);

	image = new Gtk.Image.from_pixbuf (off);

	label = new Gtk.Label ("");
	label.set_alignment (0, 0);

	pack_start(image, false, false, 10);
	pack_start(label, true, true, 0);
   }

   /**
    * Set the label
    */
   public void set_label (string label, bool use_markup = true) {
	this.label.use_markup = use_markup;
	this.label.set_label (label);
   }

   /**
    * Set the state of this TickerBox
    */
   public void set_complete (bool complete) {
	if (complete) {
		image.set_from_pixbuf (on);
	} else {
		image.set_from_pixbuf (off);
	}
   }
}
} // end namespace
