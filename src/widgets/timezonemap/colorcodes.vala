namespace Installer.Widgets {

public class ColorCodes {

   public static  string[] CODES = new string[] { "-11.0", "-10.0", "-9.5", "-9.0", "-8.0", "-7.0", "-6.0|north", "-6.0|south", "-5.0", "-4.5", "-4.0", "-3.5", "-3.0", "-2.0", "-1.0", "0.0", "1.0", "2.0", "3.0", "3.5", "4.0", "4.5", "5.0", "5.5", "5.75", "6.0", "6.5", "7.0", "8.0", "9.0", "9.5", "10.0", "10.5", "11.0", "11.5", "12.0", "13.0" };

   public static uint8[] get(string code) {
	switch (code) {
		case "-11.0":
			return new uint8[] { 43, 0, 0, 255 };
		case "-10.0":
			return new uint8[] { 85, 0, 0, 255 };
		case "-9.5":
			return new uint8[] { 102, 255, 0, 255 };
		case "-9.0":
			return new uint8[] { 128, 0, 0, 255 };
		case "-8.0":
			return new uint8[] { 170, 0, 0, 255 };
		case "-7.0":
			return new uint8[] { 212, 0, 0, 255 };
		case "-6.0|north":
			return new uint8[] { 255, 0, 1, 255 };
		case "-6.0|south":
			return new uint8[] { 255, 0, 0, 255 };
		case "-5.0":
			return new uint8[] { 255, 42, 42, 255 };
		case "-4.5":
			return new uint8[] { 192, 255, 0, 255 };
		case "-4.0":
			return new uint8[] { 255, 85, 85, 255 };
		case "-3.5":
			return new uint8[] { 0, 255, 0, 255 };
		case "-3.0":
			return new uint8[] { 255, 128, 128, 255 };
		case "-2.0":
			return new uint8[] { 255, 170, 170, 255 };
		case "-1.0":
			return new uint8[] { 255, 213, 213, 255 };
		case "0.0":
			return new uint8[] { 43, 17, 0, 255 };
		case "1.0":
			return new uint8[] { 85, 34, 0, 255 };
		case "2.0":
			return new uint8[] { 128, 51, 0, 255 };
		case "3.0":
			return new uint8[] { 170, 68, 0, 255 };
		case "3.5":
			return new uint8[] { 0, 255, 102, 255 };
		case "4.0":
			return new uint8[] { 212, 85, 0, 255 };
		case "4.5":
			return new uint8[] { 0, 204, 255, 255 };
		case "5.0":
			return new uint8[] { 255, 102, 0, 255 };
		case "5.5":
			return new uint8[] { 0, 102, 255, 255 };
		case "5.75":
			return new uint8[] { 0, 238, 207, 247 };
		case "6.0":
			return new uint8[] { 255, 127, 42, 255 };
		case "6.5":
			return new uint8[] { 204, 0, 254, 254 };
		case "7.0":
			return new uint8[] { 255, 153, 85, 255 };
		case "8.0":
			return new uint8[] { 255, 179, 128, 255 };
		case "9.0":
			return new uint8[] { 255, 204, 170, 255 };
		case "9.5":
			return new uint8[] { 70, 0, 68, 250 };
		case "10.0":
			return new uint8[] { 255, 230, 213, 255 };
		case "10.5":
			return new uint8[] { 212, 124, 21, 250 };
		case "11.0":
			return new uint8[] { 212, 170, 0, 255 };
		case "11.5":
			return new uint8[] { 249, 25, 87, 253 };
		case "12.0":
			return new uint8[] { 255, 204, 0, 255 };
		case "12.75":
			return new uint8[] { 254, 74, 100, 248 };
		case "13.0":
			return new uint8[] { 255, 85, 153, 250 };
		default:
			return new uint8[] { 0, 0, 0, 0} ;
	}
   }
} // End ColorCodes helper

} 
