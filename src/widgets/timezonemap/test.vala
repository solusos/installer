using Gtk;
using Installer.Widgets;
using Installer;

public class WidgetTest {

   private Gtk.Window window;
   private TimezoneMap map;
   private LocationDatabase db;

   /**
    * Test the callback
    */
   protected void city_selected (string zone) {
	stdout.printf("City: %s\n", zone);
   }

   public WidgetTest () {
	window = new Gtk.Window ();
	window.title = "WidgetTest -TimezoneMap";
	window.set_size_request (400, 400);

	window.set_border_width(20);

	db = new LocationDatabase ();
	map = new TimezoneMap ("../resources/timezone", db);
	map.city_selected.connect(city_selected);
	var box = new Gtk.VBox (false, 0);
	box.add (map);

	window.add(box);

	var db = new LocationDatabase ();
	window.destroy.connect (Gtk.main_quit);

	window.show_all ();
   }
}

public static void main(string[] args) {
 	Gtk.init (ref args);
	WidgetTest wt = new WidgetTest ();

	Gtk.main ();
}

