using Gtk;
using Cairo;

namespace Installer.Widgets {

public class TimezoneMap : Gtk.EventBox {

   public string imagePath { public get; construct set; }

   // Private images..
   private Gdk.Pixbuf orig_background;
   private Gdk.Pixbuf orig_color_map;

   // Currently scaled background
   private Gdk.Pixbuf background;
   private Gdk.Pixbuf color_map;

   // Our pin image
   private Gdk.Pixbuf pin_image;

   // Signal gets emitted any time a city is selected
   public signal void city_selected (string zone);

   private ColorCodes helper;

   private int visible_map_rowstride;

   // Current zone offset
   private string offset = null;

   // Our LocationDatabase..
   private LocationDatabase db;

   // Currently selected "distances"
   private List<Location> distances;

   // list index to draw
   private int index = 0;

   /**
    * Construct a new TimezoneMap
    */
   public TimezoneMap (string imagePath, LocationDatabase db) {
	this.imagePath = imagePath;

	// Init our images
	orig_background = new Gdk.Pixbuf.from_file ("%s/bg.png".printf(imagePath));
	orig_color_map = new Gdk.Pixbuf.from_file ("%s/cc.png".printf(imagePath));
	pin_image = new Gdk.Pixbuf.from_file("%s/pin_map.png".printf(imagePath));
	button_press_event.connect (button_press);
	add_events (Gdk.EventMask.BUTTON_PRESS_MASK);

	realize.connect (on_realize);
	size_allocate.connect (do_size_allocate);
	size_request.connect (do_size_request);

	helper = new ColorCodes ();

	this.db = db;

	distances = new List<Location> ();

	// Update once per second
	Timeout.add (1000, () => { this.queue_draw(); return true; });
   }

   /**
    * Rescale the background on resize
    */
   private void do_size_allocate (Gdk.Rectangle alloc) {
	background = orig_background.scale_simple (allocation.width, allocation.height, Gdk.InterpType.BILINEAR);
	color_map = orig_color_map.scale_simple (allocation.width, allocation.height, Gdk.InterpType.BILINEAR);
        visible_map_rowstride = color_map.get_rowstride();
   }

   /**
    * Attempt to set the country
    */
   public void select_country (string country ) {
	foreach (unowned Location c in db.locations) {
		if (c.country == country) {
			this.distances = new List<Location> ();
			this.distances.append(c);
			index = 0;

			var height = this.background.get_height ();
			var width = this.background.get_width ();
			var x = convert_longitude_to_x (c.longitude, width);
			var y = convert_latitude_to_y (c.latitude, height);
			this.offset = convert_xy_to_offset (x, y);
			break;
		}
	}
   }

   /**
    * Try and keep things in a nice ratio
    */
   private void do_size_request (out Gtk.Requisition req) {
	req.width = orig_background.get_width () / 2;
	req.height = orig_background.get_height () / 2;
   }

   /**
    * Now we're a real window, set a cursor
    */
   private void on_realize () {
	window.set_cursor (new Gdk.Cursor (Gdk.CursorType.HAND1));
   }

   /**
    * Our main drawing code
    */
   public override bool expose_event (Gdk.EventExpose e) {
        var cr = Gdk.cairo_create( window);


	Gdk.cairo_set_source_pixbuf (cr, background, 0, 0);
	cr.paint ();

	// Draw the offset if appropriate
	if (offset != null) {
		var pbuf = new Gdk.Pixbuf.from_file( "%s/timezone_%s.png".printf(imagePath, offset.split("|")[0]) ).scale_simple (allocation.width, allocation.height, Gdk.InterpType.BILINEAR);
		Gdk.cairo_set_source_pixbuf (cr, pbuf,  0, 0);
		cr.paint ();
	}

	// Draw locations
	if (this.distances.length() == 0) return true; // No distances to check.

	var height = this.background.get_height ();
	var width = this.background.get_width ();

	unowned Location loc = this.distances.nth_data (index);
	if (loc == null) return true;
	var pointx = convert_longitude_to_x(loc.longitude, width);
	var pointy = convert_latitude_to_y(loc.latitude, height);

	// Draw the white dot
	Gdk.Color c;
	Gdk.Color black;
	Gdk.Color.parse ("white", out c);
	Gdk.Color.parse("black", out black);
	cr.set_source_rgb (c.red, c.green, c.blue);
	cr.arc(pointx, pointy, 5.0, 0, 2 * Math.PI);
	cr.set_line_width(3.5);
	cr.fill_preserve();
	cr.set_source_rgba(black.red, black.green, black.blue, 0.85);
	cr.stroke();

	/* Draw the pin icon.
	var pinX =( pointx - (pin_image.get_width() / 2));
	//var pinY = pointy - (pin_image.get_height() / 2) + 4.5;
	var pinY = pointy - pin_image.get_height () - 7.5; // Point at the target
	Gdk.cairo_set_source_pixbuf (cr, pin_image,  pinX, pinY);
	cr.paint ();

	// Draw the box for infos.
	var newx = pointx + 20;
	var newy = pointy;*/

	var newx = pointx;
	var newy = pointy;


	TimeVal time = new TimeVal ();
	time_t timestamp = time.tv_sec;
	timestamp += loc.utc_offset;
	Time t2 = Time.gm (timestamp);

	Cairo.TextExtents t;
	string displ = loc.human_zone;
	string format = "%H:%M:%S";
	displ += t2.format(" (%s) ".printf(format));
	cr.text_extents (displ, out t);

	newy -= t.height - 30;
	newx -= (t.width / 2);

	// Now draw box..
	//CairoExtensions.rounded_rectangle (cr, 0, 0, w, h, r, CairoCorners.NO_CORNERS);
	cr.set_source_rgba (black.red, black.green, black.blue, 0.85);
	CairoExtensions.rounded_rectangle (cr, newx-10, newy-t.height-4, t.width + 20, t.height + 20, 4, CairoCorners.ALL);
	cr.fill_preserve ();
	//cr.stroke ();
	cr.move_to (newx, newy+5);
	cr.set_line_width (0.0);
	cr.set_source_rgb (c.red, c.green, c.blue);
	cr.show_text (displ);
	cr.stroke ();

	return true;
   }
  
   /**
    * Clicked on the map
    */
   protected bool button_press (Gdk.EventButton event) {
	stdout.printf("X: %f, Y: %f\n", event.x, event.y);
	var toffset = convert_xy_to_offset (event.x, event.y);

	if (toffset == null) {
		// Don't now unset the timezone segment highlight
		return true;
	}

	if (toffset != this.offset) {
		index = 0;
		this.offset = toffset;

		// Find all Location's within this offset
		distances = new List<Location> ();
		foreach (unowned Location c in db.locations) {
			if (c.utc_offset == -1000) continue;
			double l_offset = (c.utc_offset / 60) / 60; // approx (hours = / minutes / seconds)
			double o_offset = toffset.to_double ();
			if (l_offset != o_offset) continue;
			distances.append (c);
		}
	}
	if (index +1 > this.distances.length()) {
		index = 0;
	} else {
		index+= 1;
	}

	// if we got a selection.. emit it
	if (this.distances.length() > 0) {
		unowned Location c = this.distances.nth_data(index);
		this.city_selected (c.zone);
	} else {
		this.city_selected (null);
	}
	queue_draw ();
	return true;
   }

   /** Convert the X, Y location to a timezone offset */
   protected string convert_xy_to_offset (double x_in, double y_in) {
        var rowstride = this.visible_map_rowstride;
	int x = (int)x_in;
	int y = (int)y_in;

	int t = (rowstride * y + x * 4);
	try {
		uint8[] tmp = new uint8[] { color_map.get_pixels()[t], color_map.get_pixels()[t+1], color_map.get_pixels()[t+2], color_map.get_pixels()[t+3] };
		foreach (string code in ColorCodes.CODES) {
	
			uint8[] comp = helper[code];
			if (comp[0] == tmp[0] && comp[1] == tmp[1] && comp[2] == tmp[2] && comp[3] == tmp[3]) {
				return code;
			}
		}
			
	} catch (GLib.Error e) {
		message (e.message);
	}

	return null;
   }

   /** Utility, Convert longitude to X coordinate */
   protected double convert_longitude_to_x (double longitude, double map_width) {
	var xdeg_offset = -6;
	var x = (map_width * (180.0 + longitude) / 360.0) + (map_width * xdeg_offset / 180.0);
	x %= map_width;
	return x;
   }

   /** Utility method - convert degrees to radians */
   protected double radians (double degrees) { return degrees * Math.PI / 180; }

   /** Utility: Convert latitude to Y coordinate */
   protected double convert_latitude_to_y (double latitude, double map_height) {
	var bottom_lat = -59;
	var top_lat = 81;
	double top_per = top_lat / 180.0;
	var y = 1.25 * Math.log (Math.tan (Math.PI / 4.0 + 0.4 * radians (latitude)));
	var full_range = 4.6068250867599998;
	double top_offset = full_range * top_per;
	var map_range = Math.fabs (1.25 * Math.log (Math.tan (Math.PI / 4.0 + 0.4 * radians (bottom_lat))) - top_offset);
	y = Math.fabs(y - top_offset);
	y /= map_range;
	y *= map_height;
	return y;
    }


} // End TimezoneMap class definiton
} // End namespace
	
