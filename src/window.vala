using Gtk;
using Cairo;
using Gdk;

using Installer.Widgets;

//using Ped; // Parted
namespace Installer { // Start namespace

public class InstallerWindow {


   // Main Window on screen
   private Gtk.Window window;

   // Gdk Pixbuf for the background image
   private Gdk.Pixbuf background_image;

   // Main navigation view
   private Gtk.Notebook notebook;

   // Content layout
   private Gtk.VBox main_box;
   private Gtk.HBox window_boxen;

   // Header label.
   private Gtk.Label header;

   // Box to hold the ticks
   private Gtk.VBox tickplace;

   // Where we keep our modules so they don't become unref'd
   private HashTable<string,InstallerModule> modules;

   // Currently selected module
   private unowned InstallerModule currentModule;

   // Navigation buttons
   private Gtk.Button forward;
   private Gtk.Button back;

   // Probably a better way of handling this.
   private HashTable<string,Gtk.Label> sections;

   // Progress Dialog
   private Dialog progress_dialog;
   private Label progress_label;
   private Gtk.Image progress_image;
   private Gtk.HBox pbox;

   // Bling mode?
   private bool blingy;

   /**
    * Construct a new InstallerWindow
    */

   public InstallerWindow() {
	GETTEXT_INIT ();
	window = new Gtk.Window ();
	window.destroy.connect( Gtk.main_quit );

	// Module storage
	modules = new HashTable<string,InstallerModule> (str_hash, str_equal);

	// Sections storage
	sections = new HashTable<string,Gtk.Label> (str_hash, str_equal);

	// Firstly, set ourselves up with the requested theme options
	string theme = Config["Interface"]["GTK-Theme"];
	string icon_theme = Config["Interface"]["Icon-Theme"];

	// Set it with GtkSettings
	var settings = window.get_settings();
	settings.set_string_property("gtk-theme-name", theme, "window.vala:57");
	settings.set_string_property("gtk-icon-theme-name", icon_theme, "window.vala:58");

	string distName = Config["Branding"]["Name"];
	window.title = "%s Installer".printf(distName);

	// Do we want bling ?
	blingy = Config["Interface"]["Bling-Default"].to_bool ();
	if (blingy) {
		get_pixbuf (Config["Interface"]["Wallpaper"], out background_image );
		// Allows Cairo drawing to take place *properly*
		window.set_app_paintable (true);
		window.expose_event.connect(exposed);
	}


	// This is just the sidepane (tick area)
	var side_boxen = new PrettyBox ();
	if (blingy) {
		string logo_url = Config["Interface"]["Logo"];
		Gtk.Image image = null;
		get_image(logo_url, out image);
		// Only place image in sidebar if in bling mode
		side_boxen.pack_start (image, false, false, 30);
	} 

	// Construct tick place holder
	tickplace = new Gtk.VBox (false, 10);
	tickplace.set_border_width (8);
	side_boxen.pack_start (tickplace, false, false, 0);

	// Main layout
	window_boxen = new Gtk.HBox (false, 20);
	window_boxen.pack_start (side_boxen, false, false, 0);
	main_box = new Gtk.VBox (false, 0);
	main_box.pack_end (window_boxen, true, true, 0);
	window.add(main_box);

	// Load the pages
	load_pages ();

	if (blingy)
		window_boxen.set_border_width (10);

	// Finally show the window (Centered)
	window.window_position = WindowPosition.CENTER;
	window.icon_name = "system-run";
	window.set_auto_startup_notification (true);

	window.size_allocate.connect( () => { 
		window.queue_draw();
	});

	// Before we show this dude, we need to set a size..
	var screen = Gdk.Screen.get_default ();

	int removal = 0;
	#if WITH_GNOME_PANEL_HACK
	var conf = GConf.Client.get_default ();

	try {
		SList list = conf.get_list ("/apps/panel/general/toplevel_id_list", GConf.ValueType.STRING);
		list.foreach ( (k) => {
			string s = (string)k;
			var panel = "/apps/panel/toplevels/%s".printf(s);
			// /apps/panel/toplevels/bottom_panel_screen0/orientation
			var orientation_ = "%s/orientation".printf(panel);
			var autohide_ = "%s/auto_hide".printf(panel);
			var size_ = "%s/size".printf(panel);

			var orientation = conf.get_string(orientation_);
			var autohide = conf.get_bool(autohide_);
			if ( (orientation == "bottom" || orientation == "top") && !autohide) {
				var size = conf.get_int (size_);
				removal += size;
			}

		} );

	} catch (GLib.Error e) { }
	#else
	removal = 20;
	#endif

	if (screen.get_height () < 768) {
		// Need to adjust layout ideally.. for smaller screen (hide side-bar? )
		window.set_size_request (800, 600);
	} else {
		// Removes -20 if no gnome-panel-hack, otherwise detects correct amount
		if (screen.get_height () <= 768) {
			window.set_size_request (1024, 768-removal);
		} else {
			// Enough room to show 1024x768 without clipping
			window.set_size_request (1024, 768);
		}
	} 

	setup_dialogs ();
	window.show_all ();
   }

   /**
    * Utility function, load a page
    */
   private void load_module (string name, string classname) {
	var mod = new InstallerModule(name);
	mod.load ();
	InstallerPage page = (InstallerPage) mod.make_new (classname);
	page.navigate_update.connect (update_navigation_state);
	page.progress_update.connect (update_progress);
	page.init_page ();

	string fgColor = Config["Interface"]["Foreground"];

	// See if the tickerbox holder already shows this section or add if not.
	// Add to tickerbox
	Gtk.Label section = sections.lookup (page.get_section() );
	if (section == null) {
		section = new Gtk.Label ("<span color=\"%s\"><b><big>%s</big></b></span>".printf(fgColor, page.get_section ()));
		section.use_markup = true;
		section.set_alignment (0, 0);
		tickplace.add(section);
		sections.insert (page.get_section(), section);
	}

	var tbox = new TickerBox ();
	tbox.set_label ("<span color=\"%s\"><big>%s</big></span>".printf(fgColor, page.get_short_title()) );
	tickplace.add (tbox);

	notebook.append_page (page.get_display (), null);

	mod.page_number = modules.size ();
	tbox.page_number = modules.size ();
	modules.insert(name, mod);

	if (currentModule == null) {
		currentModule = modules.lookup (name);
		set_current_page ((int)currentModule.page_number);
	}
   }

   /**
    * This callback is for when a page wants to let navigation state change
    */
   protected void update_navigation_state (NavigationState state, NavigationDirection direction) {
	switch (direction) {
		case NavigationDirection.FORWARD:
			if (state == NavigationState.ENABLE) {
				forward.set_sensitive (true);
			} else {
				forward.set_sensitive (false);
			}
			break;
		default:
			if (state == NavigationState.ENABLE) {
				back.set_sensitive (true);
			} else {
				back.set_sensitive (false);
			}
			break;
	}
   }

   /**
    * Our own assistance helper dohickey for changing pages
    */
   protected void set_current_page (int page_number) {
	forward.set_sensitive (false);

	if (page_number <= 0) {
		// First page - kill off back
		back.set_sensitive (false);
	} else {
		back.set_sensitive (true);
	}

	foreach (string name in modules.get_keys() ) {
		unowned InstallerModule m = modules.lookup (name);
		if (m == null) {
			continue;
		}
		if (m.page_number == page_number) {
			// Found the new "currentModule"
			currentModule = m;
			notebook.set_current_page (page_number);
			m.payload.init_page ();
			if (m.payload.get_page_type () == PageType.SUMMARY) {
				handle_summary (ref m.payload);
			}
			string fgColor = Config["Interface"]["Foreground"];
			if (blingy) {
				header.set_markup ("<big><span color=\"%s\">%s</span></big>".printf(fgColor, m.payload.get_title()));
			} else {
				header.set_markup ("<big>%s</big>".printf(m.payload.get_title()));
			}
		}
	}

	// Update the tickers
	foreach (weak Gtk.Widget child in tickplace.get_children ()) {
		if (child.get_type() != typeof(TickerBox) ) continue;
		unowned TickerBox tb = (TickerBox)child;
		if (tb.page_number <= page_number) {
			tb.set_complete (true);
		} else {
			tb.set_complete (false);
		}
	}

	if (notebook.get_current_page() == modules.size() -1) {
		// Last page, disable button
		forward.set_sensitive (false);
	}
	window.queue_draw ();
   }

   /**
    * Time to populate the summary page
    */
   protected void handle_summary (ref InstallerPage summary_page) {
	foreach (string name in modules.get_keys() ) {
		unowned InstallerModule m = modules.lookup (name);
		if (m.payload.get_page_type () == PageType.DATA) {
			summary_page.process_data (m.payload.get_short_title (), m.payload.get_data (), set_current_page, (int)m.page_number);
		}
	}
   }

   /**
    * Load all the relevant pages into the installer UI
    */
   private void load_pages () {
	// Construct the navigation stuffs
	var layout = new PrettyBox ();
	layout.set_border_width (15);
	// Create header.
	header = new Gtk.Label ("");
	header.justify = Justification.LEFT;

	header.use_markup = true;
	if (blingy) {
		header.set_alignment (0,0);
	} else {
		header.set_alignment (0.01f, 0.5f);
	}

	if (!blingy) {
		var tbar = new Gtk.Toolbar ();
		main_box.pack_start (tbar, false, false, 0);

		var item = new Gtk.ToolItem ();
		item.set_expand (true);
		item.add (header);
		tbar.add (item);

		string logo_url = Config["Interface"]["Logo"];
		Gtk.Image image = null;
		get_image(logo_url, out image);

		var item_img = new Gtk.ToolItem ();
		item_img.add (image);
		tbar.add (item_img);


	} else {
		layout.pack_start(header, false, false, 10);
	}
	// Separator
	var sep = new Gtk.HSeparator ();
	layout.pack_start (sep, false, false, 0);

	notebook = new Gtk.Notebook ();
	notebook.set_show_tabs (false);
	if (blingy) {
		// Allow more GTK-like look when no bling
		notebook.set_show_border (false);
	}
	layout.pack_start (notebook, true, true, 0);
	window_boxen.pack_start (layout, true, true, 0);

	// Buttons..
	var sep2 = new Gtk.HSeparator ();
	layout.pack_start (sep2, false, false, 5);

	var tmp = new Gtk.VBox (false, 0);
	var btnBox = new Gtk.HButtonBox ();
	btnBox.set_layout (ButtonBoxStyle.END);
	back = new Gtk.Button.from_stock (STOCK_GO_BACK);
	back.set_sensitive (false);
	back.focus_on_click = false;
	back.clicked.connect ( () => {
		var page = notebook.get_current_page ();
		page-=1;
		this.set_current_page (page);
	} );
	forward = new Gtk.Button.from_stock (STOCK_GO_FORWARD);
	forward.clicked.connect ( () => {
		var page = notebook.get_current_page ();
		page+=1;
		this.set_current_page (page);
	} );
	forward.set_sensitive (false);
	forward.focus_on_click = false;
	btnBox.add(back);
	btnBox.add(forward);
	tmp.add(btnBox);
	layout.pack_end(tmp, false, false, 0);

	load_module ("welcomepage", "InstallerWelcomePage");
	load_module ("languagepage", "InstallerLanguagePage");
	load_module ("timezonepage", "InstallerTimezonePage");
	load_module ("diskspage", "InstallerDisksPage");

	// Summary page, rather important.
	load_module ("summarypage", "InstallerSummaryPage");
   }

   /**
    * Draw the background image to the window
    */
   public bool exposed (Gtk.Widget widget, Gdk.EventExpose event ) {
	// Create our context
	var context = Gdk.cairo_create( window.window );

	// Draw it
	context.rectangle(event.area.x , event.area.y, event.area.width, event.area.height);
	context.clip();

	Gdk.cairo_set_source_pixbuf (context, background_image, 0, 0);
	context.paint ();

	return false;
   }

   /** Create our dialogs */
   private void setup_dialogs () {
	// Progress Dialog
	progress_dialog = new Dialog ();
	progress_dialog.title = _("Loading");
	progress_dialog.set_transient_for (window);
	progress_dialog.deletable = false;
	progress_label = new Gtk.Label ("<b>%s</b>".printf(_("Loading")));
	progress_label.use_markup = true;
	progress_image = new Gtk.Image.from_file ("%s/spinner.gif".printf(RESOURCE_DIR));
	pbox = new Gtk.HBox (false, 10);
	pbox.set_border_width (20);
	pbox.pack_start(progress_image, false, false, 10);
	pbox.pack_start(progress_label, true, true, 0);
	progress_dialog.vbox.pack_start (pbox, true, true, 0);
	progress_dialog.vbox.show_all ();
   }

   /**
    * Our callback for progress update calls
    */
   protected void update_progress (ProgressState st, string msg = null, float pct = null) {
	if (st == ProgressState.SHOW ) {
		progress_label.set_label ("<big>%s</big>".printf(msg));
		// We just ignore pct for now
		progress_dialog.run ();
	} else {
		progress_dialog.response (ResponseType.OK);
		progress_dialog.hide ();
	}
	window.queue_draw ();
   }
} // End InstallerWindow definition


} // End namespace
