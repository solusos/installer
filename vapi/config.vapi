[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern const string PREFIX;


[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern const string RESOURCE_DIR;


[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern const string MODULE_DIR;

[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern const string GETTEXT_PACKAGE;

[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern const string LOCALE_DIR;

[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename="config.h")]
public extern int GETTEXT_INIT();
