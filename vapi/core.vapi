namespace Posix {
	[CCode (cheader_filename = "unistd.h,sys/types.h")]
	public int seteuid (uid_t uid);
}
